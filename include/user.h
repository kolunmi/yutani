#ifndef USER_H_
#define USER_H_

#include <stdbool.h>
#include <stdint.h>
#include <wayland-server.h>
#include <xkbcommon/xkbcommon.h>
#include <wlr/types/wlr_keyboard.h>

#define MAX_KEYS 5

// TODO: Maybe we make views be dynamic?
// As in different views can be set for different monitors?
// So in the monitor rules the user can pass in a config_view
// array that can then be used for specifically that monitor.
// This could be really customizable but just as simple.

struct layout;
struct monitor;
union arg;

typedef void (*layout_func)(struct monitor *monitor);
typedef void (*bind_func)(const union arg *arg);

/* Supposed to be a nice meeting point between worspaces from i3/sway and
 * tags from dwm/dwl. They are treated largely like tags but can act like workspaces.
 * Views can either be monitor exclusive or universal.
 * Views can also be summoned on compositor start or upon application launch and so forth.
 */
struct tag_config {
    const char *name;
    const struct layout *layout; // default layout for this tag
    uint32_t master_amount; // The amount of clients to be put in the master stack
    float master_factor; // The percentage of the screen the master stack will take up.
};

struct layout {
    const char *symbol; // the displayed symbol / name of the layout
    layout_func arrange;
};

// To use an app rule you must to specify a title or an appid (or both), and a view name. The monitor name is optional.
// For example:
// { "firefox", NULL, "eDP-1", "1" },
struct app_rule {
    const char *title, *appid, *monitor;
    bool floating, fullscreen;
    uint32_t tags;
};

// To use a monitor rule you must specify a monitor name then the master amount,
// along with a master multiplier, a scale, and a transform. Optionally you may
// specify an x and y. Note that a monitor name may not be specified, this rule
// will act as a default value, the first NULL monitor name encountered will be used
// as the default.
// If x and y aren't specified then the position of the monitor is determined by the
// wlr_output_layout_add_auto() function.
// The master_amount is the amount of clients held in the master stack, and the multiplier
// is the amount of space that the master stack takes up.
// For example:
// { "eDP-1", 1, 0.55, 1, 0, 0, WL_OUTPUT_TRANSFORM_NORMAL },
struct monitor_rule {
    const char *monitor;
    float scale;
    int32_t x, y;
    enum wl_output_transform transform;
};

union arg {
    uint32_t ui;
    int32_t i;
    float f;
    const char *str;
    const void *v;
};

struct key {
    uint32_t mods;
    xkb_keysym_t keysym;
};

// A keychord
struct keybind {
    uint32_t keys_len;
    struct key keys[MAX_KEYS];
    bind_func func;
    const union arg arg;
};

enum click_location {
    CLICK_NONE, // If none of the below, ie. the background of the environment
    CLICK_CLIENT,
    CLICK_TAG,
    CLICK_LAYOUT,
    CLICK_TITLE,
    CLICK_STATUS,
};

struct buttonbind {
    // TODO:
    uint32_t mods, button;
    bind_func func;
    enum click_location click;
    const union arg arg;
};

enum color_scheme {
    SCHEME_NORMAL   = 0,
    SCHEME_SELECTED = 1,
    SCHEME_URGENT   = 2,
};

enum scheme_color {
    COLOR_FOREGROUND = 0,
    COLOR_BACKGROUND = 1,
    COLOR_BORDER     = 2,
};

void tile(struct monitor *monitor);
void monocle(struct monitor *monitor);
void vertical(struct monitor *monitor);
void quit(const union arg *arg);
void view(const union arg *arg);
void toggle_view(const union arg *arg);
void tag(const union arg *arg);
void toggle_tag(const union arg *arg);
void toggle_bar(const union arg *arg);
void toggle_floating(const union arg *arg);
void set_layout(const union arg *arg);
void client_kill(const union arg *arg);
void spawn(const union arg *arg);
void monitor_focus(const union arg *arg);

#endif // USER_H_
