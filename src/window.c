#include <string.h>
#include <wayland-util.h>
#include <wlr/render/allocator.h>
#include <wlr/util/log.h>
#include <wlr/types/wlr_scene.h>
#include "window.h"
#include "buffer.h"
#include "util.h"

static void frame_done(struct wl_listener *listener, void *data);

void frame_done(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "frame done");
    struct basic_window *window = wl_container_of(listener, window, listeners.frame_done);

    window->interface->draw(window);

    wlr_log(WLR_DEBUG, "%s", window->scene->node.enabled ? "enabled" : "not enabled");
    wlr_log(WLR_DEBUG, "%s", window->scene_buffer->node.enabled ? "enabled" : "not enabled");

    wlr_scene_buffer_set_dest_size(window->scene_buffer, window->pending_buffer->width, window->pending_buffer->height);
    wlr_scene_buffer_set_buffer(window->scene_buffer, window->pending_buffer);
    struct wlr_buffer *tmp = window->current_buffer;
    window->current_buffer = window->pending_buffer;
    window->pending_buffer = tmp;

    wlr_log(WLR_DEBUG, "%s", !pixman_region32_empty(&window->scene->node.visible) ? "visible" : "invisible");
}

void allocator_destroy(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "allocator destroy");
    struct basic_window *window = wl_container_of(listener, window, listeners.frame_done);
    basic_window_destroy(window);
}

bool basic_window_init(struct basic_window *window, const struct basic_window_interface *interface, const struct wlr_drm_format *format, struct wlr_scene_tree *parent) {
    if (!window || !interface || !format || !parent) return false;

    memset(window, 0, sizeof(*window));

    window->scene = wlr_scene_tree_create(parent);
    if (!window->scene) {
        wlr_log(WLR_ERROR, "Failed to create scene tree for basic window");
        return false;
    }

    window->scene_buffer = wlr_scene_buffer_create(window->scene, NULL);
    if (!window->scene_buffer) {
        wlr_log(WLR_ERROR, "Failed to create a scene buffer for basic window");
        wlr_scene_node_destroy(&window->scene->node);
        return false;
    }

    window->allocator = buffer_allocator_create();
    window->base.type = WINDOW_BASIC;
    window->interface = interface;
    window->format    = format;

    listen(&window->scene_buffer->events.frame_done, &window->listeners.frame_done, frame_done);

    return true;
}

bool basic_window_force_draw(struct basic_window *window) {
    if (!window || !window->scene->node.enabled) return false;

    wlr_log(WLR_DEBUG, "force draw");

    frame_done(&window->listeners.frame_done, NULL);

    return true;
}

struct wlr_buffer *basic_window_get_buffer(struct basic_window *window, uint32_t width, uint32_t height) {
    if (!window || !width || !height) return NULL;

    if (window->pending_buffer && width == window->pending_buffer->width && height == window->pending_buffer->height) {
        return window->pending_buffer;
    }

    if (window->pending_buffer) wlr_buffer_drop(window->pending_buffer);
    window->pending_buffer = wlr_allocator_create_buffer(window->allocator, width, height, window->format);
    return window->pending_buffer ? window->pending_buffer : NULL;
}

bool basic_window_destroy(struct basic_window *window) {
    if (!window) return false;

    if (window->pending_buffer) wlr_buffer_drop(window->pending_buffer);
    if (window->current_buffer) wlr_buffer_drop(window->current_buffer);
    wlr_scene_node_destroy(&window->scene->node);
    wlr_allocator_destroy(window->allocator);
    wl_list_remove(&window->listeners.frame_done.link);

    return true;
}
