#ifndef WINDOW_H_
#define WINDOW_H_

#include <stdint.h>
#include <wayland-server-core.h>
#include <wlr/util/box.h>
#include <wlr/render/drm_format_set.h>
#include "swapchain.h"

struct basic_window;

enum window_type {
    WINDOW_BAR,
    WINDOW_CLIENT,
};

// Essentially just an opaque type that is used
// to differentiate between different internal node types
// within yutani and apply safe type casting conventions using
// wl_container_of
struct window {
    enum window_type type;
};

struct basic_window_interface {
    void (*draw)(struct basic_window *window);
};

struct bar {
    struct window base;
    struct wlr_allocator *allocator;
    struct swapchain swapchain;
};

struct basic_window {
    struct window base;
    struct wlr_allocator *allocator;
    const struct basic_window_interface *interface;
    const struct wlr_drm_format *format;
    struct wlr_scene_tree *scene;
    struct wlr_scene_buffer *scene_buffer;
    struct wlr_buffer *current_buffer, *pending_buffer;

    struct {
        struct wl_listener frame_done;
    } listeners;
};

bool basic_window_init(struct basic_window *window, const struct basic_window_interface *interface, const struct wlr_drm_format *format, struct wlr_scene_tree *parent);
bool basic_window_force_draw(struct basic_window *window);
struct wlr_buffer *basic_window_get_buffer(struct basic_window *window, uint32_t width, uint32_t height);
bool basic_window_destroy(struct basic_window *window);

#endif // WINDOW_H_
