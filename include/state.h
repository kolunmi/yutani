#ifndef STATE_H_
#define STATE_H_

#include <fcft/fcft.h>
#include <stdint.h>
#include <wlr/backend.h>
#include <wlr/types/wlr_scene.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_idle_notify_v1.h>
#include <wlr/types/wlr_keyboard_shortcuts_inhibit_v1.h>
#include <wlr/types/wlr_gamma_control_v1.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_cursor_shape_v1.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_keyboard_group.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/xwayland.h>
#include <wlr/util/box.h>
#include <wlr/xcursor.h>
#include <wayland-server.h>
#include <xkbcommon/xkbcommon.h>
#include "window.h"
#include "wlr-layer-shell-unstable-v1-protocol.h"
#include "user.h"

#define WLR_LAYER_SHELL_LAYERS 4

struct tag_config;

enum cursor_mode {
    CURSOR_MODE_RELEASED = 0,
    CURSOR_MODE_PRESSED,
    CURSOR_MODE_MOVE,
    CURSOR_MODE_RESIZE,
};

enum layers {
    BACKGROUND = 0,
    BOTTOM     = 1,
    TILED      = 2,
    FLOATING   = 3,
    FULLSCREEN = 4,
    TOP        = 5,
    OVERLAY    = 6,
    LOCK       = 7,
    NUM_LAYERS,
};

struct pointer {
    struct state *state;
    struct wlr_pointer *pointer;

    struct {
        struct wl_listener destroy;
    } listeners;

    struct wl_list link;
};

struct keyboard {
    struct keyboard_group *group;
    struct wlr_keyboard *keyboard;

    struct {
        struct wl_listener destroy;
    } listeners;

    struct wl_list link;
};

struct keyboard_group {
    struct state *state;
    struct wlr_keyboard_group *group;
    struct wl_list keyboards; // struct keyboard

    struct key prev_keys[MAX_KEYS];
    const xkb_keysym_t *keysyms;
    uint32_t mods, prev_keys_len, keysyms_len;
    struct wl_event_source *key_repeat;

    struct {
        struct wl_listener key;
        struct wl_listener modifiers;
    } listeners;
};

struct tag {
    const struct tag_config *config;
    const struct layout *layout[2];
    uint32_t current_layout;
    char layout_symbol[16];
    float master_factor;
    uint32_t master_amount;
    bool show_bar;
};

struct monitor {
    struct state *state;
    struct wlr_output *output;
    struct wlr_scene_output *output_scene;
    struct wlr_box monitor_space, toplevel_space;
    struct wlr_session_lock_surface_v1 *lock_surface;
    struct client_toplevel *current_toplevel;
    struct basic_window indicator;

    struct utl_vector tags; // struct tag
    struct tag *current_tag;
    uint32_t tagset[2],
             active_tags,  // TODO: This might not be needed.
             current_tagset;

    struct {
        struct wl_listener frame;
        struct wl_listener request_state;
        struct wl_listener destroy;
        struct wl_listener lock_surface_destroy;
    } listeners;

    struct wl_list link;
};

struct state {
    struct wl_display *display;
    struct wlr_backend *backend;
    struct wlr_renderer *renderer;
    struct wlr_allocator *allocator;
    struct wlr_output_layout *output_layout;
    struct wlr_cursor *cursor;
    struct wlr_xcursor_manager *xcursor;
    struct wlr_seat *seat;
    struct wlr_xwayland *xwayland;

    struct wlr_scene *scene;
    struct wlr_box scene_geometry;
    struct wlr_scene_rect *root_background;
    struct wlr_scene_tree *layers[NUM_LAYERS],
                          *drag_icon;

    struct fcft_font *font;

    struct keyboard_group keyboards, virtual_keyboards;
    struct wl_list pointers; // struct pointer

    struct wl_list monitors; // struct monitor
    struct wl_list toplevels; // struct client_toplevel.link; the list of mapped clients
    struct wl_list focus_stack; // struct client_toplevel.focus_stack; the focust list of mapped clients
    struct wl_list layered_clients[WLR_LAYER_SHELL_LAYERS]; // struct client_layered.link

    struct monitor *selected_monitor; // can be NULL
    struct client *selected_client;   // can be NULL

    enum cursor_mode cursor_mode;

    struct {
        struct wlr_session_lock_v1 *lock;
        struct wlr_scene_tree *surfaces;
        struct wlr_scene_rect *background;
    } session_lock;
    bool locked;

    struct {
        struct wlr_cursor_shape_manager_v1 *cursor_shape;
        struct wlr_idle_notifier_v1 *idle_notifier;
        struct wlr_idle_inhibit_manager_v1 *idle_inhibit;
        struct wlr_keyboard_shortcuts_inhibit_manager_v1 *shortcuts_inhibitor;
        struct wlr_gamma_control_manager_v1 *gamma_control;
        struct wlr_compositor *compositor;
    } globals;

    // TODO: We need to actually set up all of these listeners
    // As well as start laying out the infrastructure of the rest of the compositor.
    struct {
        struct wl_listener new_output;
        struct wl_listener new_input;
        struct wl_listener new_xdg_client;
        struct wl_listener new_layer_client;
        struct wl_listener new_xwayland_client;
        struct wl_listener new_idle_inhibitor;
        struct wl_listener new_decoration;
        struct wl_listener new_lock;
        struct wl_listener new_virtual_keyboard;
        struct wl_listener xdg_activation_activate;
        struct wl_listener gamma_changed;
        struct wl_listener lock_manager_destroy;
        struct wl_listener output_layout_change;
        struct wl_listener output_manager_apply;
        struct wl_listener output_manager_test;
        struct wl_listener cursor_motion;
        struct wl_listener cursor_motion_absolute;
        struct wl_listener cursor_button;
        struct wl_listener cursor_axis;
        struct wl_listener cursor_frame;
        struct wl_listener cursor_shape_set_request;
        struct wl_listener seat_set_cursor;
        struct wl_listener seat_set_selection;
        struct wl_listener seat_set_primary_selection;
        struct wl_listener seat_set_request_start_drag;
        struct wl_listener seat_set_start_drag;
        struct wl_listener drag_icon_destroy;
        struct wl_listener xwayland_ready;
        struct wl_listener new_lock_surface;
        struct wl_listener lock_destroy;
        struct wl_listener unlock;
    } listeners;
};

#endif // STATE_H_
