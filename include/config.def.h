#ifndef CONFIG_H_
#define CONFIG_H_

#include "user.h"
#include <stdint.h>
#include <xkbcommon/xkbcommon-keysyms.h>

static const uint32_t gray1        = 0x222222ff;
static const uint32_t gray2        = 0x444444ff;
static const uint32_t gray3        = 0xbbbbbbff;
static const uint32_t gray4        = 0xeeeeeeff;
static const uint32_t cyan         = 0x005577ff;
static const uint32_t locked_color = 0x191919ff;
static const uint32_t root_color   = gray1;
static const uint32_t colors[][3]  = {
    [SCHEME_NORMAL] = {
        [COLOR_FOREGROUND] = gray3,
        [COLOR_BACKGROUND] = gray1,
        [COLOR_BORDER]     = gray2,
    },
    [SCHEME_SELECTED] = {
        [COLOR_FOREGROUND] = gray4,
        [COLOR_BACKGROUND] = cyan,
        [COLOR_BORDER]     = cyan,
    },
    [SCHEME_URGENT] = {
        [COLOR_FOREGROUND] = gray1,
        [COLOR_BACKGROUND] = gray3,
        [COLOR_BORDER]     = cyan,
    },
};

static const char *fonts[] = {
    "monospace:size=13",
};


static const struct layout layouts[] = {
    { "[]=", tile },
    { "[M]", monocle },
    { "|||", vertical},
    { "><>", NULL },
};

static const struct tag_config tags[] = {
    { "1", &layouts[0], 1, 0.55 },
    { "2", &layouts[0], 1, 0.55 },
    { "3", &layouts[0], 1, 0.55 },
    { "4", &layouts[0], 1, 0.55 },
    { "5", &layouts[0], 1, 0.55 },
    { "6", &layouts[0], 1, 0.55 },
    { "7", &layouts[0], 1, 0.55 },
    { "8", &layouts[0], 1, 0.55 },
    { "9", &layouts[0], 1, 0.55 },
};

static const struct app_rule app_rules[] = {};

static const struct monitor_rule monitor_rules[] = {
    { NULL, 1, -1, -1, WL_OUTPUT_TRANSFORM_NORMAL },
};

#define MODIFIER WLR_MODIFIER_ALT
#define TAGKEYS(KEY, SHIFT_KEY, TAG) \
    { 1, {{0, (KEY)}},                                            view,        {.ui = (1 << (TAG))} }, \
    { 1, {{WLR_MODIFIER_CTRL,  (KEY)}},                           toggle_view, {.ui = (1 << (TAG))} }, \
    { 1, {{WLR_MODIFIER_SHIFT, (SHIFT_KEY)}},                     tag,         {.ui = (1 << (TAG))} }, \
    { 1, {{WLR_MODIFIER_SHIFT|WLR_MODIFIER_CTRL, (SHIFT_KEY)}},   toggle_tag,  {.ui = (1 << (TAG))} } \

static const char *menu[] = {"bemenu-run", NULL};
static const char *term[] = {"alacritty", NULL};

static const struct keybind keybinds[] = {
    {1, {{0, XKB_KEY_p}},                       spawn,           {.v = menu}},
    {1, {{WLR_MODIFIER_SHIFT, XKB_KEY_Return}}, spawn,           {.v = term}},
    {1, {{WLR_MODIFIER_SHIFT, XKB_KEY_Q}},      quit,            {0}},
    {1, {{0, XKB_KEY_0}},                       view,            {.ui = ~0}},
    {1, {{WLR_MODIFIER_SHIFT, XKB_KEY_0}},      tag,             {.ui = ~0}},
    {1, {{0, XKB_KEY_b}},                       toggle_bar,      {0}},
    {1, {{0, XKB_KEY_t}},                       set_layout,      {.v = &layouts[0]}},
    {1, {{0, XKB_KEY_m}},                       set_layout,      {.v = &layouts[1]}},
    {1, {{0, XKB_KEY_v}},                       set_layout,      {.v = &layouts[2]}},
    {1, {{0, XKB_KEY_f}},                       set_layout,      {.v = &layouts[3]}},
    {1, {{0, XKB_KEY_space}},                   set_layout,      {0}},
    {1, {{WLR_MODIFIER_SHIFT, XKB_KEY_space}},  toggle_floating, {0}},
    {1, {{WLR_MODIFIER_SHIFT, XKB_KEY_C}},      client_kill,     {0}},
	TAGKEYS(XKB_KEY_1, XKB_KEY_exclam,          0),
	TAGKEYS(XKB_KEY_2, XKB_KEY_at,              1),
	TAGKEYS(XKB_KEY_3, XKB_KEY_numbersign,      2),
	TAGKEYS(XKB_KEY_4, XKB_KEY_dollar,          3),
	TAGKEYS(XKB_KEY_5, XKB_KEY_percent,         4),
	TAGKEYS(XKB_KEY_6, XKB_KEY_asciicircum,     5),
	TAGKEYS(XKB_KEY_7, XKB_KEY_ampersand,       6),
	TAGKEYS(XKB_KEY_8, XKB_KEY_asterisk,        7),
	TAGKEYS(XKB_KEY_9, XKB_KEY_parenleft,       8),
};
static const struct buttonbind buttonbinds[] = {};

static const uint32_t border_width = 1;
static const int32_t keyboard_repeat_rate  = 25,
                     keyboard_repeat_delay = 600;

#endif // CONFIG_H_
