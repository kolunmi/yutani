#include <assert.h>
#include <bits/time.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <drm_fourcc.h>
#include <pixman.h>
#include <math.h>
#include <fcft/fcft.h>
#include <fcft/stride.h>
#include <wayland-server-core.h>
#include <wayland-server-protocol.h>
#include <wayland-util.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/render/allocator.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_subcompositor.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/types/wlr_export_dmabuf_v1.h>
#include <wlr/types/wlr_screencopy_v1.h>
#include <wlr/types/wlr_data_control_v1.h>
#include <wlr/types/wlr_primary_selection_v1.h>
#include <wlr/types/wlr_viewporter.h>
#include <wlr/types/wlr_single_pixel_buffer_v1.h>
#include <wlr/types/wlr_fractional_scale_v1.h>
#include <wlr/types/wlr_xdg_activation_v1.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_idle_notify_v1.h>
#include <wlr/types/wlr_keyboard_shortcuts_inhibit_v1.h>
#include <wlr/types/wlr_idle_inhibit_v1.h>
#include <wlr/types/wlr_gamma_control_v1.h>
#include <wlr/types/wlr_session_lock_v1.h>
#include <wlr/types/wlr_server_decoration.h>
#include <wlr/types/wlr_xdg_decoration_v1.h>
#include <wlr/types/wlr_xdg_output_v1.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_output_management_v1.h>
#include <wlr/types/wlr_presentation_time.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_cursor_shape_v1.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_virtual_keyboard_v1.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_primary_selection.h>
#include <wlr/types/wlr_scene.h>
#include <wlr/types/wlr_keyboard_group.h>
#include <wlr/xwayland.h>
#include <wlr/util/log.h>
#include <wayland-server.h>
#include <wlr/xcursor.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xproto.h>
#include <xkbcommon/xkbcommon.h>
#include "client.h"
#include "swapchain.h"
#include "user.h"
#include "utf8.h"
#include "util.h"
#include "window.h"
#include "config.h"
#include "buffer.h"

#define END(array) ((array) + length(array))
#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define CLEAN_MOD_MASK(mods) (mods & ~WLR_MODIFIER_CAPS)
#define TAGMASK ((1 << length(tags)) - 1)
#define CLIENT_VISIBLE_ON(client, mon) ((mon) && (client)->monitor == (mon) && (client)->tags & (mon)->tagset[(mon)->current_tagset])
#define STRING_EQUAL(str1, str2) (strcmp(str1, str2) == 0)
#define WLR_LAYER_SHELL_LAYERS 4
#define LAYOUT_SYMBOL_MAX 16
#define DEFAULT_STATUS ("yutani " VERSION)

static_assert(length(tags) <= 32, "There must be less than 32 tags");
static_assert(length(layouts) != 0, "There must be at least 1 layout (if you don't want a tile like layout just set a NULL arrange function in a layout)");

enum net_wm_atom {
    NetWMWindowTypeDialog,
    NetWMWindowTypeSplash,
    NetWMWindowTypeToolbar,
    NetWMWindowTypeUtility,
    NetLast,
};

enum cursor_mode {
    CURSOR_MODE_RELEASED = 0,
    CURSOR_MODE_PRESSED,
    CURSOR_MODE_MOVE,
    CURSOR_MODE_RESIZE,
};

enum layers {
    BACKGROUND = 0,
    BOTTOM     = 1,
    TILED      = 2,
    FLOATING   = 3,
    FULLSCREEN = 4,
    TOP        = 5,
    OVERLAY    = 6,
    LOCK       = 7,
    NUM_LAYERS,
};

struct pointer {
    struct wlr_pointer *pointer;

    struct {
        struct wl_listener destroy;
    } listeners;

    struct wl_list link;
};

struct keyboard {
    struct keyboard_group *group;
    struct wlr_keyboard *keyboard;

    struct {
        struct wl_listener destroy;
    } listeners;

    struct wl_list link;
};

struct keyboard_group {
    struct wlr_keyboard_group *group;
    struct wl_list keyboards; // struct keyboard

    struct key prev_keys[MAX_KEYS];
    const xkb_keysym_t *keysyms;
    uint32_t mods, prev_keys_len, keysyms_len;
    struct wl_event_source *key_repeat;

    struct {
        struct wl_listener key;
        struct wl_listener modifiers;
    } listeners;
};

struct tag {
    const struct tag_config *config;
    const struct layout *layouts[2];
    uint32_t layout;
    char layout_symbol[LAYOUT_SYMBOL_MAX];
    float master_factor;
    uint32_t master_amount;
    bool show_bar;
    struct wl_list link;
};

struct monitor {
    struct wlr_output *output;
    struct wlr_scene_output *output_scene;
    struct wlr_box monitor_space, toplevel_space;
    struct wlr_session_lock_surface_v1 *lock_surface;
    struct toplevel *current_toplevel; // can be NULL

    struct wl_list tags; // struct tag
    struct tag *current_tag;
    uint32_t tagset[2],
             active_tags,  // TODO: This might not be needed.
             current_tagset;

    struct {
        struct window base;
        struct wlr_scene_buffer *scene;
        struct swapchain swapchain;
        char *status; // TODO: Status whenever
    } bar;

    struct {
        struct wl_listener frame;
        struct wl_listener request_state;
        struct wl_listener destroy;
        struct wl_listener lock_surface_destroy;
        struct wl_listener frame_done;
    } listeners;

    struct wl_list link;
};

static void box_apply_bounds(struct wlr_box *box, struct wlr_box *bound);
static bool buttonbind(uint32_t button);
static void cleanup(void);
static void client_apply_rules(struct toplevel *toplevel);
static void client_focus(struct toplevel *toplevel);
static bool client_notify_enter(struct toplevel *toplevel, struct wlr_keyboard *keyboard);
static void client_resize(struct toplevel *toplevel, struct wlr_box *bound, struct wlr_box *geometry);
static void client_set_floating(struct toplevel *toplevel, bool floating);
static void client_set_fullscreen(struct toplevel *toplevel, bool fullscreen);
static void client_set_monitor(struct toplevel *toplevel, struct monitor *monitor, uint32_t tags);
static void client_unfocus(struct toplevel *toplevel);
static bool client_wants_floating(struct toplevel *toplevel);
static void cursor_axis(struct wl_listener *listener, void *data);
static void cursor_button(struct wl_listener *listener, void *data);
static void cursor_frame(struct wl_listener *listener, void *data);
static void cursor_motion(struct wl_listener *listener, void *data);
static void cursor_motion_absolute(struct wl_listener *listener, void *data);
static void cursor_motion_handle(uint32_t time); // TODO: We might need some other stuff here
static void cursor_shape_set_request(struct wl_listener *listener, void *data);
static void decoration_destroy(struct wl_listener *listener, void *data);
static void decoration_request(struct wl_listener *listener, void *data);
static void drag_icon_destroy(struct wl_listener *listener, void *data);
static void draw_bar(struct wl_listener *listener, void *data);
static void gamma_changed(struct wl_listener *listener, void *data);
static bool keybind(struct keyboard_group *group, const xkb_keysym_t keysym, uint32_t mods);
static void keyboard_destroy(struct wl_listener *listener, void *data);
static void keyboard_init(struct keyboard_group *group, struct wlr_keyboard *keyboard);
static void keyboard_key(struct wl_listener *listener, void *data);
static void keyboard_modifiers(struct wl_listener *listener, void *data);
static int32_t keyboard_repeat(void *data);
static struct toplevel *toplevel_try_from_window(struct window *window);
static struct window *window_from_xy(uint32_t x, uint32_t y, double *nx, double *ny);
static void xdg_activation_activate(struct wl_listener *listener, void *data);
static void setup(void);
static void run(void);
static void new_output(struct wl_listener *listener, void *data);
static void new_input(struct wl_listener *listener, void *data);
static void new_xdg_client(struct wl_listener *listener, void *data);
static void new_layer_client(struct wl_listener *listener, void *data);
static void new_xwayland_client(struct wl_listener *listener, void *data);
static void new_idle_inhibitor(struct wl_listener *listener, void *data);
static void new_decoration(struct wl_listener *listener, void *data);
static void new_lock(struct wl_listener *listener, void *data);
static void new_virtual_keyboard(struct wl_listener *listener, void *data);
static void lock_manager_destroy(struct wl_listener *listener, void *data);
static void output_layout_change(struct wl_listener *listener, void *data);
static void output_manager_apply(struct wl_listener *listener, void *data);
static void output_manager_test(struct wl_listener *listener, void *data);
static void seat_set_cursor(struct wl_listener *listener, void *data);
static void seat_set_selection(struct wl_listener *listener, void *data);
static void seat_set_primary_selection(struct wl_listener *listener, void *data);
static void seat_set_request_start_drag(struct wl_listener *listener, void *data);
static void seat_set_start_drag(struct wl_listener *listener, void *data);
static void xwayland_ready(struct wl_listener *listener, void *data);
static void monitor_frame(struct wl_listener *listener, void *data);
static void monitor_destroy(struct wl_listener *listener, void *data);
static void monitor_request_state(struct wl_listener *listener, void *data);
static void pointer_init(struct wlr_pointer *wlr_pointer);
static void pointer_destroy(struct wl_listener *listener, void *data);
static void new_lock_surface(struct wl_listener *listener, void *data);
static void session_lock_destroy(struct wl_listener *listener, void *data);
static void unlock(struct wl_listener *listener, void *data);
static void lock_destroy();
static void lock_surface_destroy(struct wl_listener *listener, void *data);
static void toplevel_commit(struct wl_listener *listener, void *data);
static void toplevel_destroy(struct wl_listener *listener, void *data);
static void toplevel_map(struct wl_listener *listener, void *data);
static void toplevel_unmap(struct wl_listener *listener, void *data);
static void toplevel_fullscreen(struct wl_listener *listener, void *data);
static void toplevel_maximize(struct wl_listener *listener, void *data);
static struct monitor *monitor_from_position(uint32_t x, uint32_t y);
static void window_draw(struct basic_window *window);
static uint32_t text_draw(struct fcft_font *font, pixman_image_t *image, const char *text, const pixman_color_t *text_color, const struct wlr_box *box);
static uint32_t text_width(struct fcft_font *font, const char *text);
static void monitor_set_bar(struct monitor *monitor, bool enabled); // TODO: Whenever I get to it
static void update_cursor();
static void monitor_update_geometry(struct monitor *monitor);
static struct toplevel *monitor_get_top(struct monitor *monitor);
static void seat_update_capabilities(void);
static void monitor_arrange(struct monitor *monitor);
static void monitor_update_tag(struct monitor *monitor);
static struct monitor *monitor_from_xy(uint32_t x, uint32_t y);

static struct wl_display *display;
static struct wlr_backend *backend;
static struct wlr_renderer *renderer;
static struct wlr_allocator *allocator;
static struct wlr_output_layout *output_layout;
static struct wlr_cursor *cursor;
static struct wlr_xcursor_manager *xcursor;
static struct wlr_seat *seat;
static struct wlr_xwayland *xwayland;

static struct wlr_scene *scene;
static struct wlr_box scene_geometry;
static struct wlr_scene_rect *root_background;
static struct wlr_scene_tree *layers[NUM_LAYERS],
                             *drag_icon;

static struct fcft_font *font;

static struct keyboard_group keyboards, virtual_keyboards;
static struct wl_list pointers; // struct pointer

static struct wl_list monitors; // struct monitor
static struct wl_list toplevels; // struct client_toplevel.link; the list of mapped clients
static struct wl_list focus_stack; // struct client_toplevel.focus_stack; the focust list of mapped clients
static struct wl_list layered_clients[WLR_LAYER_SHELL_LAYERS]; // struct client_layered.link

static struct monitor *selected_monitor; // can be NULL

static enum cursor_mode cursor_mode;

static struct {
    struct wlr_session_lock_v1 *lock;
    struct wlr_scene_tree *surfaces;
    struct wlr_scene_rect *background;
} session_lock;
static bool locked = false;

static struct {
    struct wlr_cursor_shape_manager_v1 *cursor_shape;
    struct wlr_idle_notifier_v1 *idle_notifier;
    struct wlr_idle_inhibit_manager_v1 *idle_inhibit;
    struct wlr_keyboard_shortcuts_inhibit_manager_v1 *shortcuts_inhibitor;
    struct wlr_gamma_control_manager_v1 *gamma_control;
    struct wlr_compositor *compositor;
} globals;

static struct {
    struct wl_listener new_output;
    struct wl_listener new_input;
    struct wl_listener new_xdg_client;
    struct wl_listener new_layer_client;
    struct wl_listener new_xwayland_client;
    struct wl_listener new_idle_inhibitor;
    struct wl_listener new_decoration;
    struct wl_listener new_lock;
    struct wl_listener new_virtual_keyboard;
    struct wl_listener xdg_activation_activate;
    struct wl_listener gamma_changed;
    struct wl_listener lock_manager_destroy;
    struct wl_listener output_layout_change;
    struct wl_listener output_manager_apply;
    struct wl_listener output_manager_test;
    struct wl_listener cursor_motion;
    struct wl_listener cursor_motion_absolute;
    struct wl_listener cursor_button;
    struct wl_listener cursor_axis;
    struct wl_listener cursor_frame;
    struct wl_listener cursor_shape_set_request;
    struct wl_listener seat_set_cursor;
    struct wl_listener seat_set_selection;
    struct wl_listener seat_set_primary_selection;
    struct wl_listener seat_set_request_start_drag;
    struct wl_listener seat_set_start_drag;
    struct wl_listener drag_icon_destroy;
    struct wl_listener xwayland_ready;
    struct wl_listener new_lock_surface;
    struct wl_listener lock_destroy;
    struct wl_listener unlock;
} listeners;

static const struct wlr_drm_format argb32 = {.format = DRM_FORMAT_ARGB8888, .len = 1, .modifiers = (uint64_t[]){DRM_FORMAT_MOD_LINEAR}};
static const char broken[] = "broken"; // for clients with null titles / appids
static xcb_atom_t netatom[NetLast];

void box_apply_bounds(struct wlr_box *box, struct wlr_box *bound) {
    // try and fit the box into the bounds if we can
    box->width  = MAX(1, box->width);
    box->height = MAX(1, box->height);

    if (box->x >= bound->x + bound->width) box->x = bound->x + bound->width - box->width;
    if (box->y >= bound->y + bound->height) box->y = bound->y + bound->height - box->height;
    if (box->x + box->width <= bound->x) box->x = bound->x;
    if (box->y + box->height <= bound->y) box->y = bound->y;
}

bool buttonbind(uint32_t button) {
    enum click_location location = CLICK_NONE;
    uint32_t mods = CLEAN_MOD_MASK(wlr_keyboard_get_modifiers(seat->keyboard_state.keyboard));

    double x, y;
    struct window *window = window_from_xy(cursor->x, cursor->y, &x, &y);
    if (!window) goto done;

    if (window->type == WINDOW_CLIENT) {
        location = CLICK_CLIENT;
        goto done;
    }

    uint32_t n = 0, i = 0;
    struct tag *tag;
    wl_list_for_each(tag, &selected_monitor->tags, link) {
        if (x <= n) {
            location = CLICK_TAG;
            goto done;
        }
        n += text_width(font, tag->config->name) + font->height;
        i++;
    }

    n += text_width(font, selected_monitor->current_tag->layout_symbol) + font->height;
    if (x <= n) {
        location = CLICK_LAYOUT;
        goto done;
    }

    uint32_t status_width = text_width(font, selected_monitor->bar.status ? selected_monitor->bar.status : DEFAULT_STATUS) + font->height;
    n += selected_monitor->monitor_space.width - n - status_width;
    if (x <= n) {
        location = CLICK_TITLE;
        goto done;
    }

    if (x > n && x <= selected_monitor->monitor_space.width) location = CLICK_STATUS;

    // TODO: Reconstruct status location based on how we render it later

done:

    for (const struct buttonbind *bind = buttonbinds; bind < END(buttonbinds); bind++) {
        if (button != bind->button || mods != bind->mods || location != bind->click || !bind->func) continue;

        union arg arg = bind->arg;
        if (location == CLICK_TAG && !arg.i) arg.ui = 1 << i;
        bind->func(&arg);

        return true;
    }

    return false;
}

void cleanup(void) {
    wlr_xwayland_destroy(xwayland);
    wl_display_destroy_clients(display);
    wlr_xcursor_manager_destroy(xcursor);
    wlr_output_layout_destroy(output_layout);
    wl_display_destroy(display);
    wlr_scene_node_destroy(&scene->tree.node);
    fcft_destroy(font);
    fcft_fini();
}

void client_apply_rules(struct toplevel *toplevel) {
    wlr_log(WLR_DEBUG, "client apply rules");
    if (!toplevel) return;

    const char *title = client_get_title(toplevel);
    if (!title) title = broken;

    const char *appid = client_get_appid(toplevel);
    if (!appid) appid = broken;

    struct monitor *monitor = selected_monitor;
    uint32_t tags = monitor->tagset[monitor->current_tagset];
    bool floating = client_wants_floating(toplevel);

    for (int i = 0; i < length(app_rules); i++) {
        const struct app_rule *rule = &app_rules[i];
        if ((rule->title && !STRING_EQUAL(rule->title, title)) && (rule->appid && !STRING_EQUAL(rule->appid, appid))) continue;

        tags = rule->tags ? rule->tags : tags;
        floating = rule->floating;

        struct monitor *pos;
        wl_list_for_each(pos, &monitors, link) {
            if (!STRING_EQUAL(rule->monitor, pos->output->name)) continue;
            tags = pos->tagset[pos->current_tagset];
            monitor = pos;
            break;
        }

        break;
    }

    // TODO: Reparent to floating / fullscreen / tile whenever we feel like.
    wlr_log(WLR_DEBUG, "%p", monitor);
    wlr_log(WLR_DEBUG, "%p", selected_monitor);
    client_set_monitor(toplevel, monitor, tags);
}

void client_focus(struct toplevel *toplevel) {
    if (locked || (toplevel && toplevel == selected_monitor->current_toplevel)) return;
    wlr_log(WLR_DEBUG, "client focus %p", toplevel);

    if (!toplevel) {
        wl_list_for_each(toplevel, &focus_stack, focus_link) {
            if (CLIENT_VISIBLE_ON(toplevel, selected_monitor)) break;
        }
        if (wl_list_empty(&focus_stack) || !CLIENT_VISIBLE_ON(toplevel, selected_monitor)) toplevel = NULL;
    }

    if (toplevel && !toplevel->monitor) {
        wlr_log(WLR_DEBUG, "toplevel doesn't have monitor sorry");
        return;
    }
    if ((toplevel && !client_wants_focus(toplevel)) || !toplevel) client_unfocus(selected_monitor->current_toplevel);
    if (toplevel && !client_is_unmanaged(toplevel)) {
        if (toplevel == selected_monitor->current_toplevel) return;
        wl_list_remove(&toplevel->focus_link);
        wl_list_insert(&focus_stack, &toplevel->focus_link);
        selected_monitor = toplevel->monitor;
        toplevel->urgent = false;
        client_restack(toplevel);

        if (!seat->drag /* &&  TODO: Exclusive client at some point*/) {
            client_set_border(toplevel, colors[SCHEME_SELECTED][COLOR_BORDER]);
        }
        client_notify_enter(toplevel, wlr_seat_get_keyboard(seat));
        client_set_activated(toplevel, true);
    }

    selected_monitor->current_toplevel = toplevel;

    // TODO: Motion notify?

    // TODO: Check and see if we can activate the client by checking if the previous surface is a layered client
}

void client_kill(const union arg *arg) {
    if (!selected_monitor->current_toplevel) return;
    client_send_close(selected_monitor->current_toplevel);
}

bool client_notify_enter(struct toplevel *toplevel, struct wlr_keyboard *keyboard) {
    if (!toplevel) return false;
    wlr_seat_keyboard_notify_enter(seat,
            toplevel->xdg.surface->surface,
            keyboard ? keyboard->keycodes : NULL,
            keyboard ? keyboard->num_keycodes : 0,
            keyboard ? &keyboard->modifiers : NULL);
    return true;
}

void client_resize(struct toplevel *toplevel, struct wlr_box *geometry, struct wlr_box *bound) {
    if (!toplevel || !geometry) return;

    client_set_bounds(toplevel, geometry->width, geometry->height);
    if (bound) box_apply_bounds(geometry, bound);
    toplevel->geometry = *geometry;

    wlr_scene_node_set_position(&toplevel->scene->node, toplevel->geometry.x, toplevel->geometry.y);
    wlr_scene_node_set_position(&toplevel->surface->node, toplevel->border_width, toplevel->border_width);
    wlr_scene_node_set_position(&toplevel->border[0]->node, 0, 0);
    wlr_scene_node_set_position(&toplevel->border[1]->node, 0, 0);
    wlr_scene_node_set_position(&toplevel->border[2]->node, 0, toplevel->geometry.height - toplevel->border_width);
    wlr_scene_node_set_position(&toplevel->border[3]->node, toplevel->geometry.width - toplevel->border_width, 0);
    wlr_scene_rect_set_size(toplevel->border[0], toplevel->geometry.width, toplevel->border_width);
    wlr_scene_rect_set_size(toplevel->border[1], toplevel->border_width, toplevel->geometry.height);
    wlr_scene_rect_set_size(toplevel->border[2], toplevel->geometry.width, toplevel->border_width);
    wlr_scene_rect_set_size(toplevel->border[3], toplevel->border_width, toplevel->geometry.height);

    toplevel->resize_serial = client_set_size(toplevel, toplevel->geometry.width - (toplevel->border_width * 2),
            toplevel->geometry.height - (toplevel->border_width * 2));

    struct wlr_box clip;
    client_get_clip(toplevel, &clip);
    wlr_scene_subsurface_tree_set_clip(&toplevel->surface->node, &clip);
}

void client_set_floating(struct toplevel *toplevel, bool floating) {
    if (!toplevel || toplevel->fullscreen) return;

    toplevel->floating = floating;
    if (toplevel->floating) wlr_scene_node_reparent(&toplevel->scene->node, layers[FLOATING]);
    monitor_arrange(toplevel->monitor);
}

void client_set_fullscreen(struct toplevel *toplevel, bool fullscreen) {
    // TODO: This later at some point. Doesn't seem super important right now
}

void client_set_monitor(struct toplevel *toplevel, struct monitor *monitor, uint32_t tags) {
    wlr_log(WLR_DEBUG, "client set monitor");
    if (!toplevel || toplevel->monitor == monitor || !monitor) return;

    struct monitor *previous_monitor = toplevel->monitor;
    if (previous_monitor) monitor_arrange(previous_monitor);

    toplevel->monitor = monitor;
    toplevel->tags = tags ? tags : toplevel->monitor->tagset[toplevel->monitor->current_tagset];
    client_focus(toplevel);
    monitor_arrange(toplevel->monitor);
}

void client_unfocus(struct toplevel *toplevel) {
    if (!toplevel || !toplevel->monitor || toplevel != selected_monitor->current_toplevel || client_is_unmanaged(toplevel)) return;
    wlr_log(WLR_DEBUG, "client unfocus %p", toplevel);

    if (client_is_xdg(toplevel)) {
        struct wlr_xdg_popup *popup, *tmp;
        wl_list_for_each_safe(popup, tmp, &toplevel->xdg.surface->popups, link)
            wlr_xdg_popup_destroy(popup);
    }

    client_set_border(toplevel, colors[SCHEME_NORMAL][COLOR_BORDER]);
    client_set_activated(toplevel, false);

    wlr_seat_keyboard_notify_clear_focus(seat);

    selected_monitor->current_toplevel = NULL;
}

bool client_wants_floating(struct toplevel *toplevel) {
    if (!toplevel) return false;
    if (client_is_x11(toplevel)) {
        struct wlr_xwayland_surface *surface = toplevel->x11.surface;
        xcb_size_hints_t *size_hints = surface->size_hints;
        if (toplevel->x11.surface->modal) return true;

        for (int i = 0; i < surface->window_type_len; i++) {
            if (surface->window_type[i] == netatom[NetWMWindowTypeDialog]
                    || surface->window_type[i] == netatom[NetWMWindowTypeSplash]
                    || surface->window_type[i] == netatom[NetWMWindowTypeToolbar]
                    || surface->window_type[i] == netatom[NetWMWindowTypeUtility])
                return true;
        }

        return size_hints && size_hints->min_width > 0 &&
            size_hints->min_height > 0 &&
            (size_hints->max_width == size_hints->min_width ||
             size_hints->max_height == size_hints->min_height);
    }

    struct wlr_xdg_toplevel *top = toplevel->xdg.surface->toplevel;
	return top->parent ||
        (top->current.min_width != 0 &&
         top->current.min_height != 0 &&
         (top->current.min_width == top->current.max_width ||
          top->current.min_height == top->current.max_height));
}

void cursor_axis(struct wl_listener *listener, void *data) {
    struct wlr_pointer_axis_event *event = data;
    wlr_seat_pointer_notify_axis(seat, event->time_msec, event->orientation, event->delta, event->delta_discrete, event->source);
}

void cursor_button(struct wl_listener *listener, void *data) {
    struct wlr_pointer_button_event *event = data;
    bool handled = false;

    // TODO: idle notifier you know when

    switch (event->state) {
        case WLR_BUTTON_PRESSED:
            cursor_mode = CURSOR_MODE_PRESSED;
            handled = buttonbind(event->button);
            break;
        case WLR_BUTTON_RELEASED:
            cursor_mode = CURSOR_MODE_RELEASED;
            // TODO: Reset
            break;
    }

    if (handled) return;

    wlr_seat_pointer_notify_button(seat, event->time_msec, event->button, event->state);
}

void cursor_frame(struct wl_listener *listener, void *data) {
    wlr_seat_pointer_notify_frame(seat);
}

void cursor_motion(struct wl_listener *listener, void *data) {
    struct wlr_pointer_motion_event *event = data;
    wlr_cursor_move(cursor, &event->pointer->base, event->delta_x, event->delta_y);
    cursor_motion_handle(event->time_msec);
}

void cursor_motion_absolute(struct wl_listener *listener, void *data) {
    struct wlr_pointer_motion_absolute_event *event = data;
    wlr_cursor_warp_absolute(cursor, &event->pointer->base, event->x, event->y);
    cursor_motion_handle(event->time_msec);
}

void cursor_motion_handle(uint32_t time) {
    // TODO: We need to handle the fact that a resize and move can happen. Plus a idle notification. Plus notify the client.
    // Use window_from_xy toplevel_try_from_window to set the client focus
    // and monitor_from_xy to set the selected monitor.
    // Then use client_notify_enter and client_notify_motion
    // And set the cursor image if we aren't focused on anything
    // NOW!
    // Then later handle resizing and client moving and idle notifier
    // Also add support or layered clients in notify

    struct monitor *monitor = monitor_from_xy(cursor->x, cursor->y);
    if (monitor != selected_monitor) {
        client_unfocus(selected_monitor->current_toplevel);
        selected_monitor = monitor;
        client_focus(monitor->current_toplevel);
    }

    double x, y;
    struct window *window = window_from_xy(cursor->x, cursor->y, &x, &y);
    if (!window || window->type != WINDOW_CLIENT) {
        wlr_cursor_set_xcursor(cursor, xcursor, "default");
        wlr_seat_pointer_clear_focus(seat);
        return;
    }

    struct toplevel *toplevel = toplevel_try_from_window(window);
    if (!toplevel) return;

    client_focus(toplevel);
    wlr_seat_pointer_notify_enter(seat, client_get_surface(toplevel), x, y);
    wlr_seat_pointer_notify_motion(seat, time, x, y);
}

void cursor_shape_set_request(struct wl_listener *listener, void *data) {
    struct wlr_cursor_shape_manager_v1_request_set_shape_event *event = data;
    if (seat->pointer_state.focused_client == event->seat_client) wlr_cursor_set_xcursor(cursor, xcursor, wlr_cursor_shape_v1_name(event->shape));
}

void decoration_destroy(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.decoration_destroy);
    wl_list_remove(&toplevel->listeners.decoration_destroy.link);
    wl_list_remove(&toplevel->listeners.decoration_request.link);
}

void decoration_request(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.decoration_request);
    wlr_xdg_toplevel_decoration_v1_set_mode(toplevel->xdg.decoration, WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE);
}

void drag_icon_destroy(struct wl_listener *listener, void *data) {
    /* TODO: Focus enter isn't sent during drag, so refocus the focused node. */
    wl_list_remove(&listeners.drag_icon_destroy.link);
}

void draw_bar(struct wl_listener *listener, void *unused_data) {
    struct monitor *monitor = wl_container_of(listener, monitor, listeners.frame_done);

    const char *status = monitor->bar.status ? monitor->bar.status : DEFAULT_STATUS;
    const uint32_t height = font->height + 2,
          width = monitor->monitor_space.width,
          boxs = font->height / 9,
          boxw = font->height / 6 + 1,
          // Magic calculation straight from my butt
          bottom_pad = ((height + font->descent) / 2) + (height / 6);
    enum color_scheme scheme;
    pixman_color_t background, foreground;
    struct wlr_buffer *buffer = swapchain_get(&monitor->bar.swapchain, width, height);

    void *data;
    size_t stride;
    uint32_t format;
    wlr_buffer_begin_data_ptr_access(buffer, WLR_BUFFER_DATA_PTR_ACCESS_WRITE, &data, &format, &stride);

    pixman_image_t *image = pixman_image_create_bits(PIXMAN_a8r8g8b8, monitor->monitor_space.width, font->height, data, stride);

    // TODO: Draw status first to be overwritten by other stuff later

    struct toplevel *toplevel;
    uint32_t occupied = 0, urgent = 0, tagset = monitor->tagset[monitor->current_tagset], i = 0, x = 0;
    wl_list_for_each(toplevel, &toplevels, link) {
        if (toplevel->monitor != monitor) continue;

        occupied |= toplevel->tags;
        if (toplevel->urgent) urgent |= toplevel->tags;
    }

    // tags
    struct tag *tag;
    wl_list_for_each(tag, &monitor->tags, link) {
        uint32_t tag_width = text_width(font, tag->config->name) + font->height;
        scheme = (urgent & 1 << i ? SCHEME_URGENT : (tagset & 1 << i ? SCHEME_SELECTED : SCHEME_NORMAL));
        background = PIXMAN_COLOR(colors[scheme][COLOR_BACKGROUND]), foreground = PIXMAN_COLOR(colors[scheme][COLOR_FOREGROUND]);

        pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
                &(pixman_box32_t){
                .x1 = x, .x2 = x + tag_width,
                .y1 = 0, .y2 = height,
                });

        if (occupied & 1 << i) {
            pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &foreground, 1,
                &(pixman_box32_t){
                .x1 = x + boxs, .x2 = x + boxs + boxw,
                .y1 = boxs, .y2 = boxs + boxw,
                });

            if (selected_monitor != monitor || !selected_monitor->current_toplevel || !(selected_monitor->current_toplevel->tags & 1 << i)) {
                pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
                        &(pixman_box32_t){
                        .x1 = x + boxs + 1, .x2 = x + boxs + boxw - 1,
                        .y1 = boxs + 1, .y2 = boxs + boxw - 1,
                        });
            }
        }

        text_draw(font, image, tag->config->name, &foreground,
                &(struct wlr_box){
                .x = x + (font->height / 2), .y = bottom_pad,
                .width = -1, .height = -1,
                });
        i++;
        x += tag_width;
        if (x >= width) goto done;
    }

    // layout
    background = PIXMAN_COLOR(colors[SCHEME_NORMAL][COLOR_BACKGROUND]);
    foreground = PIXMAN_COLOR(colors[SCHEME_NORMAL][COLOR_FOREGROUND]);
    uint32_t layout_width = text_width(font, monitor->current_tag->layout_symbol) + font->height;
    pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
            &(pixman_box32_t){
            .x1 = x, .x2 = x + layout_width,
            .y1 = 0, .y2 = height,
            });

    text_draw(font, image, monitor->current_tag->layout_symbol, &foreground,
            &(struct wlr_box){
            .x = x + (font->height / 2), .y = bottom_pad,
            .width = -1, .height = -1,
            });

    x += layout_width;
    if (x >= width) goto done;

    // title
    scheme = urgent & tagset ? SCHEME_URGENT : SCHEME_SELECTED;
    background = PIXMAN_COLOR(colors[scheme][COLOR_BACKGROUND]);
    foreground = PIXMAN_COLOR(colors[scheme][COLOR_FOREGROUND]);

    uint32_t status_width = text_width(font, status) + font->height;
    uint32_t title_width = width - x - status_width;

    // TODO: We will need to handle the status being too large
    pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
            &(pixman_box32_t){
            .x1 = x, .x2 = x + title_width,
            .y1 = 0, .y2 = height,
            });

    text_draw(font, image, monitor->current_toplevel ? client_get_title(monitor->current_toplevel) : NULL, &foreground,
            &(struct wlr_box){
            .x = x + (font->height / 2), .y = bottom_pad,
            .width = -1,
            .height = -1,
            });

    if (monitor->current_toplevel && monitor->current_toplevel->floating) {
        pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &foreground, 1,
                &(pixman_box32_t){
                .x1 = x + boxs, .x2 = x + boxs + boxw,
                .y1 = boxs, .y2 = boxs + boxw,
                });

        if (!client_wants_floating(monitor->current_toplevel)) {
            pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
                    &(pixman_box32_t){
                    .x1 = x + boxs + 1, .x2 = x + boxs + boxw -1,
                    .y1 = boxs + 1, .y2 = boxs + boxw - 1,
                    });
        }
    }

    if (!status_width) goto done;
    x += title_width;

    // status
    background = PIXMAN_COLOR(colors[SCHEME_NORMAL][COLOR_BACKGROUND]);
    foreground = PIXMAN_COLOR(colors[SCHEME_NORMAL][COLOR_FOREGROUND]);

    pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
            &(pixman_box32_t){
            .x1 = x, .x2 = x + status_width,
            .y1 = 0, .y2 = height,
            });

    text_draw(font, image, status, &foreground,
            &(struct wlr_box){
            .x = x + (font->height / 2), .y = bottom_pad,
            .width = -1, .height = -1
            });

done:
    wlr_buffer_end_data_ptr_access(buffer);
    wlr_scene_buffer_set_buffer(monitor->bar.scene, buffer);
    swapchain_buffer_submitted(&monitor->bar.swapchain, buffer);
    pixman_image_unref(image);
}

bool keybind(struct keyboard_group *group, const xkb_keysym_t keysym, uint32_t mods) {
    wlr_log(WLR_DEBUG, "keybind");
    mods = CLEAN_MOD_MASK(mods);

    // TODO: This might not work
    // What we need is vector of keybinds that will work with the given keys.
    // Then we trim this down over time as we get more valid keys.
    // Of course cancelling when we get one that doesn't match any.
    // And in the case where we have a keybind that is technically a match,
    // but there is another keybind that may be expanded from the current one.
    // We just use a timer to determine when the user has chosen this one.
    // Or we do none of that and just require that and just choose the first match.
    // Example for timer thing:
    // {2, {{WLR_MODIFIER_ALT, XKB_KEY_1}, {0, XKB_KEY_s}}, select, {1}},
    // {1, {{WLR_MODIFIER_ALT, XKB_KEY_1}, {0, XKB_KEY_i}}, view, {1}},
    // {1, {{WLR_MODIFIER_ALT, XKB_KEY_1}}, view, {1}},
    // Then if we get the second binding. We just make a timer and wait for another input
    // If the timer goes off then we choose the current match. But if the user interrupts with another
    // key press we continue narrowing the matches.

    if (++group->prev_keys_len == MAX_KEYS) {
        group->prev_keys_len = 0;
        return false;
    }

    const uint32_t current_key = group->prev_keys_len - 1;
    group->prev_keys[current_key].keysym = keysym;
    group->prev_keys[current_key].mods = mods;

    for (int i = 0; i < length(keybinds); i++) {
        const struct keybind *bind = &keybinds[i];

        if (group->prev_keys_len > bind->keys_len || bind->keys_len > MAX_KEYS || !bind->keys_len) continue;

        const struct key *key = &bind->keys[current_key],
                         *prev = &group->prev_keys[current_key];
        if (key->keysym != prev->keysym || key->mods != prev->mods) continue;

        if (current_key == bind->keys_len - 1 && bind->func) {
            bind->func(&bind->arg);
            group->prev_keys_len = 0;
        }
        return true;
    }

    group->prev_keys_len = 0;
    return false;
}

void keyboard_destroy(struct wl_listener *listener, void *data) {
    struct keyboard *keyboard = wl_container_of(listener, keyboard, listeners.destroy);
    wl_list_remove(&keyboard->listeners.destroy.link);
    wl_list_remove(&keyboard->link);
    if (wl_list_empty(&keyboard->group->keyboards)) seat_update_capabilities();
    free(keyboard);
}

void keyboard_init(struct keyboard_group *group, struct wlr_keyboard *wlr_keyboard) {
    if (!group || !wlr_keyboard) return;

    wlr_keyboard_set_keymap(wlr_keyboard, group->group->keyboard.keymap);
    wlr_keyboard_set_repeat_info(wlr_keyboard, keyboard_repeat_rate, keyboard_repeat_delay);
    wlr_keyboard_group_add_keyboard(group->group, wlr_keyboard);

    struct keyboard *keyboard = ezalloc(sizeof(*keyboard));
    keyboard->keyboard = wlr_keyboard;
    keyboard->group = group;
    listen(&keyboard->keyboard->base.events.destroy, &keyboard->listeners.destroy, keyboard_destroy);
    wl_list_insert(&group->keyboards, &keyboard->link);
}

void keyboard_key(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "keyboard keys");
    struct keyboard_group *group = wl_container_of(listener, group, listeners.key);
    struct wlr_keyboard_key_event *event = data;

    bool handled = false;
    uint32_t keycode = event->keycode + 8; // libinput keycode -> xkbcommon
    const xkb_keysym_t *syms;
    uint32_t syms_len = xkb_state_key_get_syms(group->group->keyboard.xkb_state, keycode, &syms);
    uint32_t mods = wlr_keyboard_get_modifiers(&group->group->keyboard);

    if (!locked && event->state == WL_KEYBOARD_KEY_STATE_PRESSED) {
        for (int i = 0; i < syms_len && !handled; i++) {
            handled = keybind(group, syms[i], mods);
        }
    }

    if (handled && group->group->keyboard.repeat_info.delay > 0) {
        group->keysyms = syms;
        group->keysyms_len = syms_len;
        group->mods = mods;
        if (wl_event_source_timer_update(group->key_repeat, group->group->keyboard.repeat_info.delay) == -1) {
            wlr_log(WLR_ERROR, "Failed to arm key repeat timer");
        }
    } else if (wl_event_source_timer_update(group->key_repeat, 0) == -1) {
        wlr_log(WLR_ERROR, "Failed to disarm key repeat timer");
    }

    if (handled) return;

    wlr_log(WLR_DEBUG, "not handled ... forwarding");
    wlr_log(WLR_DEBUG, "keyboard is focused: %s", seat->keyboard_state.focused_surface ? "true" : "false");

    wlr_seat_set_keyboard(seat, &group->group->keyboard);
    wlr_seat_keyboard_notify_key(seat, event->time_msec, event->keycode, event->state);
}

void keyboard_modifiers(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "keyboard modifiers");
    struct keyboard_group *keyboard = wl_container_of(listener, keyboard, listeners.modifiers);
    wlr_seat_set_keyboard(seat, &keyboard->group->keyboard);
    wlr_seat_keyboard_notify_modifiers(seat, &keyboard->group->keyboard.modifiers);
}

int32_t keyboard_repeat(void *data) {
    // TODO: Maybe or maybe don't who cares
    return 0;
}

void monitor_focus(const union arg *arg) {

}

void monocle(struct monitor *monitor) {
    uint32_t n = 0;
    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &focus_stack, focus_link) {
        if (CLIENT_VISIBLE_ON(toplevel, monitor)) {
            client_resize(toplevel, &monitor->toplevel_space, &monitor->toplevel_space);
            if (n == 1) wlr_scene_node_raise_to_top(&toplevel->scene->node);
            n++;
        }
    }

    if (n) snprintf(monitor->current_tag->layout_symbol, LAYOUT_SYMBOL_MAX, "[%d]", n);
}

void set_layout(const union arg *arg) {
    if (!selected_monitor) return;
    if (!arg || !arg->v || arg->v != selected_monitor->current_tag->layouts[selected_monitor->current_tag->layout]) selected_monitor->current_tag->layout ^= 1;
    if (arg->v) selected_monitor->current_tag->layouts[selected_monitor->current_tag->layout] = (struct layout*)arg->v;
    strncpy(selected_monitor->current_tag->layout_symbol, selected_monitor->current_tag->layouts[selected_monitor->current_tag->layout]->symbol, LAYOUT_SYMBOL_MAX);
    if (selected_monitor->current_toplevel) monitor_arrange(selected_monitor);
}

void spawn(const union arg *arg) {
    if (fork() == 0) {
		setsid();
		execvp(((char **)arg->v)[0], (char **)arg->v);
        panic("execvp failed: %s", ((char **)arg->v)[0]);
    }
}

void tag(const union arg *arg) {
    if (!selected_monitor->current_toplevel || !(arg->ui & TAGMASK)) return;

    selected_monitor->current_toplevel->tags = arg->ui & TAGMASK;
    client_focus(NULL);
    monitor_arrange(selected_monitor);
}

void tile(struct monitor *monitor) {
    uint32_t n = 0, i, master_width, master_y, stack_y;

    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &toplevels, link) {
        if (CLIENT_VISIBLE_ON(toplevel, monitor) && !toplevel->floating && !toplevel->fullscreen) {
            n++;
        }
    }
    if (!n) return;

    if (n > monitor->current_tag->master_amount) {
        master_width = monitor->current_tag->master_amount ? monitor->toplevel_space.width * monitor->current_tag->master_factor : 0;
    } else {
        master_width = monitor->toplevel_space.width;
    }

    i = master_y = stack_y = 0;
    wl_list_for_each(toplevel, &toplevels, link) {
        if (!CLIENT_VISIBLE_ON(toplevel, monitor) || toplevel->floating || toplevel->fullscreen) continue;

        if (i < monitor->current_tag->master_amount) {
            client_resize(toplevel, &(struct wlr_box){
                    .x = monitor->toplevel_space.x,
                    .y = monitor->toplevel_space.y + master_y,
                    .width = master_width,
                    .height = (monitor->toplevel_space.height - master_y) / (MIN(n, monitor->current_tag->master_amount) - i),
                    },
                    &monitor->toplevel_space);
            master_y += toplevel->geometry.height;
        }
        else {
            client_resize(toplevel, &(struct wlr_box){
                    .x = monitor->toplevel_space.x + master_width,
                    .y = monitor->toplevel_space.y + stack_y,
                    .width = monitor->toplevel_space.width - master_width,
                    .height = (monitor->toplevel_space.height - stack_y) / (n - i),
                    },
                    &monitor->toplevel_space);
            stack_y += toplevel->geometry.height;
        }

        i++;
    }
}


void toggle_bar(const union arg *arg) {
    if (!selected_monitor) return;
    selected_monitor->current_tag->show_bar = !selected_monitor->current_tag->show_bar;
    monitor_update_tag(selected_monitor);
    monitor_arrange(selected_monitor);
}

void toggle_floating(const union arg *arg) {
    if (!selected_monitor || !selected_monitor->current_toplevel) return;
    struct toplevel *toplevel = selected_monitor->current_toplevel;
    client_set_floating(toplevel, (!toplevel->floating || client_wants_floating(toplevel)));
}

void toggle_tag(const union arg *arg) {
    if (!selected_monitor->current_toplevel) return;

    uint32_t new_tags = selected_monitor->current_toplevel->tags ^ (arg->ui & TAGMASK);
    if (!new_tags) return;

    selected_monitor->current_toplevel->tags = new_tags;
    client_focus(NULL);
    monitor_arrange(selected_monitor);
}

struct toplevel *toplevel_try_from_window(struct window *window) {
    if (!window) return NULL;

    struct client *client = NULL;
    if (window->type != WINDOW_CLIENT) return NULL;
    client = wl_container_of(window, client, base);

    struct toplevel *toplevel = NULL;
    if (client->type != CLIENT_XDG && client->type != CLIENT_X11) return NULL;

    return (toplevel = wl_container_of(client, toplevel, base));
}

void vertical(struct monitor *monitor) {
    uint32_t n = 0;
    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &toplevels, link) {
        if (CLIENT_VISIBLE_ON(toplevel, monitor)) n++;
    }

    uint32_t width = ceil((1.0 / n) * monitor->toplevel_space.width), x = 0;
    wl_list_for_each(toplevel, &toplevels, link) {
        if (!CLIENT_VISIBLE_ON(toplevel, monitor)) continue;

        client_resize(toplevel,
                &(struct wlr_box){
                .x = x + toplevel->geometry.x, .y = toplevel->geometry.y,
                .width = width, .height = toplevel->geometry.height,
                },
                &monitor->toplevel_space);

        x += width;
    }
}

struct window *window_from_xy(uint32_t x, uint32_t y, double *nx, double *ny) {
    if (!nx || !ny) return NULL;
    struct window *window = NULL;

    for (int32_t layer = NUM_LAYERS - 1; layer >= 0; layer--) {
        struct wlr_scene_node *node = wlr_scene_node_at(&layers[layer]->node, x, y, nx, ny);
        if (!node) continue;

        for (struct wlr_scene_node *tmp = node; tmp && !window; tmp = tmp->parent ? &tmp->parent->node : NULL) {
            window = tmp->data;
        }
        if (window) break;
    }

    return window;
}

void xdg_activation_activate(struct wl_listener *listener, void *data) {
    struct wlr_xdg_activation_v1_request_activate_event *event = data;
    struct toplevel *toplevel = NULL;
    if (!root_client_from_surface(event->surface, NULL, NULL, &toplevel) || toplevel == monitor_get_top(selected_monitor)) return;

    toplevel->urgent = true;
    client_set_border(toplevel, colors[SCHEME_URGENT][COLOR_BORDER]);
}

void setup(void) {
    wlr_log(WLR_DEBUG, "setup");

    wl_list_init(&pointers);
    wl_list_init(&monitors);
    wl_list_init(&toplevels);
    wl_list_init(&focus_stack);
    for (int i = 0; i < WLR_LAYER_SHELL_LAYERS; i++) wl_list_init(&layered_clients[i]);

    assert(fcft_init(FCFT_LOG_COLORIZE_AUTO, false, FCFT_LOG_CLASS_ERROR));
    assert(fcft_set_scaling_filter(FCFT_SCALING_FILTER_LANCZOS3));
    assert((font = fcft_from_name(length(fonts), fonts, NULL)));

    assert(display = wl_display_create());
    assert(backend = wlr_backend_autocreate(display, NULL));
    listen(&backend->events.new_output, &listeners.new_output, new_output);
    listen(&backend->events.new_input, &listeners.new_input, new_input);

    assert(renderer = wlr_renderer_autocreate(backend));
    assert(allocator = wlr_allocator_autocreate(backend, renderer));

    assert(scene = wlr_scene_create());
    assert((root_background = wlr_scene_rect_create(&scene->tree, 0, 0, GLSL_COLOR(root_color))))
    for (int i = 0; i < NUM_LAYERS; i++) assert((layers[i] = wlr_scene_tree_create(&scene->tree)));


    assert(drag_icon = wlr_scene_tree_create(&scene->tree));
    wlr_scene_node_place_below(&drag_icon->node, &layers[LOCK]->node);

	/* Create shm, drm and linux_dmabuf interfaces by ourselves.
	 * The simplest way is call:
	 *      wlr_renderer_init_wl_display(drw);
	 * but we need to create manually the linux_dmabuf interface to integrate it
	 * with wlr_scene. */
	assert(wlr_renderer_init_wl_shm(renderer, display));
	if (wlr_renderer_get_dmabuf_texture_formats(renderer)) {
        struct wlr_linux_dmabuf_v1 *linux_dmabuf = wlr_linux_dmabuf_v1_create_with_renderer(display, 4, renderer);
        assert(linux_dmabuf);
		wlr_scene_set_linux_dmabuf_v1(scene, linux_dmabuf);
	}

    assert((output_layout = wlr_output_layout_create()));
    listen(&output_layout->events.change, &listeners.output_layout_change, output_layout_change);

    if (!wlr_subcompositor_create(display))                                                     wlr_log(WLR_ERROR, "Failed to create subcompositor global");
    if (!wlr_data_device_manager_create(display))                                               wlr_log(WLR_ERROR, "Failed to create data device manager global");
    if (!wlr_export_dmabuf_manager_v1_create(display))                                          wlr_log(WLR_ERROR, "Failed to create export dmabuf manager global");
    if (!wlr_screencopy_manager_v1_create(display))                                             wlr_log(WLR_ERROR, "Failed to create screencopy manager global");
    if (!wlr_data_control_manager_v1_create(display))                                           wlr_log(WLR_ERROR, "Failed to create data control manager global");
    if (!wlr_primary_selection_v1_device_manager_create(display))                               wlr_log(WLR_ERROR, "Failed to create primary selection device manager global");
    if (!wlr_viewporter_create(display))                                                        wlr_log(WLR_ERROR, "Failed to create viewporter global");
    if (!wlr_single_pixel_buffer_manager_v1_create(display))                                    wlr_log(WLR_ERROR, "Failed to create single pixel buffer manager global");
    if (!wlr_fractional_scale_manager_v1_create(display, 1))                                    wlr_log(WLR_ERROR, "Failed to create fractional scale manager global");
    if (!wlr_xdg_output_manager_v1_create(display, output_layout))                       wlr_log(WLR_ERROR, "Failed to create xdg output manager failed");
    if (!(globals.compositor = wlr_compositor_create(display, 6, renderer)))      wlr_log(WLR_ERROR, "Failed to create compositor global");
    if (!(globals.idle_notifier = wlr_idle_notifier_v1_create(display)))                 wlr_log(WLR_ERROR, "Failed to create idle notifier global");

    struct wlr_output_manager_v1 *output_manager = wlr_output_manager_v1_create(display);
    if (output_manager) {
        //listen(&output_manager->events.apply, &listeners.output_manager_apply, output_manager_apply);
        //listen(&output_manager->events.test, &listeners.output_manager_test, output_manager_test);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create wlr output manager global");
    }

    struct wlr_presentation *presentation = wlr_presentation_create(display, backend);
    if (presentation) {
        wlr_scene_set_presentation(scene, presentation);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create presentation global");
    }

    struct wlr_xdg_shell *xdg_shell = wlr_xdg_shell_create(display, 6);
    if (xdg_shell) {
        listen(&xdg_shell->events.new_surface, &listeners.new_xdg_client, new_xdg_client);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create xdg shell global");
    }

    struct wlr_layer_shell_v1 *layer_shell = wlr_layer_shell_v1_create(display, 4);
    if (layer_shell) {
        //listen(&layer_shell->events.new_surface, &listeners.new_layer_client, new_layer_client);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create layer shell global");
    }

    if ((xwayland = wlr_xwayland_create(display, globals.compositor, true))) {
        //listen(&xwayland->events.ready, &listeners.xwayland_ready, xwayland_ready);
        //listen(&xwayland->events.new_surface, &listeners.new_xwayland_client, new_xwayland_client);
        setenv("DISPLAY", xwayland->server->display_name, 1);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create xwayland server");
    }

    struct wlr_session_lock_manager_v1 *lock_manager = wlr_session_lock_manager_v1_create(display);
    // NOTE: This will be initialized later when the monitors are updated.
    session_lock.background = wlr_scene_rect_create(layers[LOCK], 0, 0, GLSL_COLOR(locked_color));
    session_lock.surfaces = wlr_scene_tree_create(layers[LOCK]);

    assert(lock_manager && session_lock.background && session_lock.surfaces);

    wlr_scene_node_set_enabled(&session_lock.background->node, false);
    wlr_scene_node_set_enabled(&session_lock.surfaces->node, false);
    //listen(&lock_manager->events.new_lock, &listeners.new_lock, new_lock);
    //listen(&lock_manager->events.destroy, &listeners.lock_manager_destroy, lock_manager_destroy);

    struct wlr_server_decoration_manager *server_decoration_manager = wlr_server_decoration_manager_create(display);
    if (server_decoration_manager) {
        wlr_server_decoration_manager_set_default_mode(server_decoration_manager, WLR_SERVER_DECORATION_MANAGER_MODE_SERVER);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create server decoration manager failed");
    }

    struct wlr_xdg_decoration_manager_v1 *decoration_manager = wlr_xdg_decoration_manager_v1_create(display);
    if (decoration_manager) {
        listen(&decoration_manager->events.new_toplevel_decoration, &listeners.new_decoration, new_decoration);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create xdg decoration manager");
    }

    if ((globals.idle_inhibit = wlr_idle_inhibit_v1_create(display))) {
        //listen(&globals.idle_inhibit->events.new_inhibitor, &listeners.new_idle_inhibitor, new_idle_inhibitor);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create idle inhibit global");
    }

    if ((globals.gamma_control = wlr_gamma_control_manager_v1_create(display))) {
        //listen(&globals.gamma_control->events.set_gamma, &listeners.gamma_changed, gamma_changed);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create gamma control manager global");
    }

    struct wlr_xdg_activation_v1 *activation = wlr_xdg_activation_v1_create(display);
    if (activation) {
        listen(&activation->events.request_activate, &listeners.xdg_activation_activate, xdg_activation_activate);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create xdg activation global");
    }

    assert(seat = wlr_seat_create(display, "seat0"));
    //listen(&seat->events.request_set_cursor, &listeners.seat_set_cursor, seat_set_cursor);
    //listen(&seat->events.request_set_selection, &listeners.seat_set_selection, seat_set_selection);
    //listen(&seat->events.request_set_primary_selection, &listeners.seat_set_primary_selection, seat_set_primary_selection);
    //listen(&seat->events.request_start_drag, &listeners.seat_set_request_start_drag, seat_set_request_start_drag);
    //listen(&seat->events.start_drag, &listeners.seat_set_start_drag, seat_set_start_drag);

    if ((globals.cursor_shape = wlr_cursor_shape_manager_v1_create(display, 1))) {
        listen(&globals.cursor_shape->events.request_set_shape, &listeners.cursor_shape_set_request, cursor_shape_set_request);
    } else  {
        wlr_log(WLR_ERROR, "Failed to create cursor shape manager global");
    }

    assert(cursor = wlr_cursor_create());
    wlr_cursor_attach_output_layout(cursor, output_layout);
    wlr_cursor_unset_image(cursor); /* Until we get a pointer later */
    listen(&cursor->events.motion, &listeners.cursor_motion, cursor_motion);
    listen(&cursor->events.motion_absolute, &listeners.cursor_motion_absolute, cursor_motion_absolute);
    listen(&cursor->events.button, &listeners.cursor_button, cursor_button);
    listen(&cursor->events.axis, &listeners.cursor_axis, cursor_axis);
    listen(&cursor->events.frame, &listeners.cursor_frame, cursor_frame);

    assert(xcursor = wlr_xcursor_manager_create(NULL, 24));
	setenv("XCURSOR_SIZE", "24", 1);

    struct wlr_virtual_keyboard_manager_v1 *virt_keyboard_manager = wlr_virtual_keyboard_manager_v1_create(display);
    if (virt_keyboard_manager) {
        //listen(&virt_keyboard_manager->events.new_virtual_keyboard, &listeners.new_virtual_keyboard, new_virtual_keyboard);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to create virtual keyboard manager global");
    }

    assert((keyboards.group = wlr_keyboard_group_create()));
    assert((virtual_keyboards.group = wlr_keyboard_group_create()));

    struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
    assert(context);
    struct xkb_keymap *keymap = xkb_keymap_new_from_names(context, NULL, XKB_KEYMAP_COMPILE_NO_FLAGS);
    assert(keymap);

    assert(wlr_keyboard_set_keymap(&keyboards.group->keyboard, keymap));
    assert(wlr_keyboard_set_keymap(&virtual_keyboards.group->keyboard, keymap));
    xkb_keymap_unref(keymap);
    xkb_context_unref(context);

    wlr_keyboard_set_repeat_info(&keyboards.group->keyboard, keyboard_repeat_rate, keyboard_repeat_delay);
    wlr_keyboard_set_repeat_info(&virtual_keyboards.group->keyboard, keyboard_repeat_rate, keyboard_repeat_delay);

    keyboards.key_repeat = wl_event_loop_add_timer(wl_display_get_event_loop(display), keyboard_repeat, &keyboards);
    virtual_keyboards.key_repeat = wl_event_loop_add_timer(wl_display_get_event_loop(display), keyboard_repeat, &virtual_keyboards);

    wl_list_init(&keyboards.keyboards);
    wl_list_init(&virtual_keyboards.keyboards);

    listen(&keyboards.group->keyboard.events.key, &keyboards.listeners.key, keyboard_key);
    listen(&keyboards.group->keyboard.events.modifiers, &keyboards.listeners.modifiers, keyboard_modifiers);
    listen(&virtual_keyboards.group->keyboard.events.key, &virtual_keyboards.listeners.key, keyboard_key);
    listen(&virtual_keyboards.group->keyboard.events.modifiers, &virtual_keyboards.listeners.modifiers, keyboard_modifiers);

    const char *socket = wl_display_add_socket_auto(display);
    assert(socket);
    setenv("WAYLAND_DISPLAY", socket, 1);
}

void run(void) {
    wlr_log(WLR_DEBUG, "run");
    assert(wlr_backend_start(backend));

    wlr_log(WLR_DEBUG, "backend start");

    //selected_monitor = state_xy_to_monitor(state, cursor->x, cursor->y);
    selected_monitor = !wl_list_empty(&monitors) ? wl_container_of(monitors.next, selected_monitor, link) : NULL;

    wl_display_run(display);
}

void new_output(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "new_output");
    struct wlr_output *output = data;

    // Small necessary preparations for the output.
    if (!wlr_output_init_render(output, allocator, renderer)) {
        wlr_log(WLR_ERROR, "Failed to initialize wlr_output renderer");
        return;
    }

    // TODO: Apply monitor rules

    struct wlr_output_mode *mode = wlr_output_preferred_mode(output);
    if (mode) wlr_output_set_mode(output, mode);
    wlr_output_enable(output, true);

    if (!wlr_output_commit(output)) {
        wlr_log(WLR_ERROR, "Initial wlr_output commit failed");
        return;
    }

    // initialize our end
    struct monitor *monitor = ezalloc(sizeof(*monitor));
    monitor->output = output;
    monitor->output->data = monitor;
    monitor->bar.base.type = WINDOW_BAR;

    assert(swapchain_init(&monitor->bar.swapchain, buffer_allocator_create(), &argb32));
    assert((monitor->bar.scene = wlr_scene_buffer_create(layers[BOTTOM], NULL)));
    monitor->bar.scene->node.data = &monitor->bar.base;

    // Setup views
    wl_list_init(&monitor->tags);
    for (int i = 0; i < length(tags); i++) {
        struct tag *tag = ezalloc(sizeof(*tag));

        tag->config = &tags[i];
        tag->layouts[0] = tag->config->layout;
        tag->layouts[1] = length(layouts) > 1 ? &layouts[1] : &layouts[0];
        tag->layout = 0;
        tag->master_amount = tag->config->master_amount;
        tag->master_factor = tag->config->master_factor;
        tag->show_bar = true;

        // Just so we don't have an empty layout symbol but this may be overwritten by the layout function
        strncpy(tag->layout_symbol, tag->layouts[0]->symbol, LAYOUT_SYMBOL_MAX);

        wl_list_insert(monitor->tags.prev, &tag->link);
    }
    monitor->tagset[monitor->current_tagset] = 1;
    monitor_update_tag(monitor);

    listen(&monitor->output->events.frame, &monitor->listeners.frame, monitor_frame);
    listen(&monitor->output->events.request_state, &monitor->listeners.request_state, monitor_request_state);
    listen(&monitor->output->events.destroy, &monitor->listeners.destroy, monitor_destroy);
    listen(&monitor->bar.scene->events.frame_done, &monitor->listeners.frame_done, draw_bar);

    wl_list_insert(&monitors, &monitor->link);

    assert((monitor->output_scene = wlr_scene_output_create(scene, monitor->output)));
    assert(wlr_output_layout_add_auto(output_layout, monitor->output));
}

void new_input(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "new_input");
    struct wlr_input_device *device = data;
    switch (device->type) {
        case WLR_INPUT_DEVICE_KEYBOARD:
            wlr_log(WLR_DEBUG, "keyboard");
            keyboard_init(&keyboards, wlr_keyboard_from_input_device(device));
            break;
        case WLR_INPUT_DEVICE_POINTER:
            wlr_log(WLR_DEBUG, "pointer");
            pointer_init(wlr_pointer_from_input_device(device));
            break;
        default:
            // TODO: The other ones.
            wlr_log(WLR_INFO, "Did not recognize input device");
            break;
    }

    seat_update_capabilities();
}

void new_xdg_client(struct wl_listener *listener, void *data) {
    struct wlr_xdg_surface *surface = data;
    struct toplevel *toplevel;
    struct layered *layered;
    enum client_type type;

    wlr_log(WLR_DEBUG, "new xdg client");

    assert(surface->role != WLR_XDG_SURFACE_ROLE_NONE);
    if (surface->role == WLR_XDG_SURFACE_ROLE_POPUP) {
        if (!root_client_from_surface(surface->surface, &type, &layered, &toplevel)) {
            wlr_log(WLR_DEBUG, "Failed to get the root client for a popup");
            return;
        }
        surface->surface->data = wlr_scene_xdg_surface_create(surface->popup->parent->data, surface);

        // TODO: Unconstrain?
        return;
    }

    // WLR_XDG_SURFACE_ROLE_TOPLEVEL
    toplevel = ezalloc(sizeof(*toplevel));
    toplevel->base.base.type = WINDOW_CLIENT;
    toplevel->base.type = CLIENT_XDG;
    toplevel->xdg.surface = surface;
    toplevel->xdg.surface->data = toplevel;
    toplevel->border_width = border_width;
    toplevel->scene = wlr_scene_tree_create(layers[TILED]);
    toplevel->scene->node.data = &toplevel->base.base;
    toplevel->surface = wlr_scene_xdg_surface_create(toplevel->scene, toplevel->xdg.surface);
    for (int i = 0; i < 4; i++) {
        assert((toplevel->border[i] = wlr_scene_rect_create(toplevel->scene, 0, 0, GLSL_COLOR(colors[SCHEME_NORMAL][COLOR_BORDER]))));
    }
    wlr_scene_node_set_enabled(&toplevel->scene->node, false);

    wlr_xdg_toplevel_set_wm_capabilities(toplevel->xdg.surface->toplevel, WLR_XDG_TOPLEVEL_WM_CAPABILITIES_FULLSCREEN);

    listen(&toplevel->xdg.surface->events.destroy, &toplevel->listeners.destroy, toplevel_destroy);
    listen(&toplevel->xdg.surface->toplevel->events.request_fullscreen, &toplevel->listeners.fullscreen, toplevel_fullscreen);
    listen(&toplevel->xdg.surface->toplevel->events.request_maximize, &toplevel->listeners.maximize, toplevel_maximize);
    listen(&toplevel->xdg.surface->surface->events.commit, &toplevel->listeners.commit, toplevel_commit);
    listen(&toplevel->xdg.surface->surface->events.map, &toplevel->listeners.map, toplevel_map);
    listen(&toplevel->xdg.surface->surface->events.unmap, &toplevel->listeners.unmap, toplevel_unmap);
}

void new_decoration(struct wl_listener *listener, void *data) {
    struct wlr_xdg_toplevel_decoration_v1 *decoration = data;
    struct toplevel *toplevel = decoration->toplevel->base->data;
    toplevel->xdg.decoration = decoration;

    listen(&decoration->events.request_mode, &toplevel->listeners.decoration_request, decoration_request);
    listen(&decoration->events.destroy, &toplevel->listeners.decoration_destroy, decoration_destroy);

    decoration_request(&toplevel->listeners.decoration_request, decoration);
}

void new_lock(struct wl_listener *listener, void *data) {
    struct wlr_session_lock_v1 *wlr_session_lock = data;
    if (session_lock.background->node.enabled) wlr_session_lock_v1_destroy(wlr_session_lock);

    wlr_scene_node_set_enabled(&session_lock.background->node, true);
    wlr_scene_node_set_enabled(&session_lock.surfaces->node, true);

    // TODO: Clear client focus

    locked = true;
    listen(&wlr_session_lock->events.new_surface, &listeners.new_lock_surface, new_lock_surface);
    listen(&wlr_session_lock->events.destroy, &listeners.lock_destroy, session_lock_destroy);
    listen(&wlr_session_lock->events.unlock, &listeners.unlock, unlock);

    wlr_session_lock_v1_send_locked(wlr_session_lock);
}

void lock_manager_destroy(struct wl_listener *listener, void *data) {
    wl_list_remove(&listeners.lock_manager_destroy.link);
    wl_list_remove(&listeners.new_lock.link);
}

void output_layout_change(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "output layout change");

    struct wlr_box box;
    wlr_output_layout_get_box(output_layout, NULL, &scene_geometry);

    wlr_scene_node_set_position(&root_background->node, box.x, box.y);
    wlr_scene_rect_set_size(root_background, box.width, box.height);

    wlr_scene_node_set_position(&session_lock.background->node, box.x, box.y);
    wlr_scene_rect_set_size(session_lock.background, box.width, box.height);

    struct monitor *monitor;
    wl_list_for_each(monitor, &monitors, link) {
        wlr_log(WLR_DEBUG, "%s", monitor->output->name);
        if (!monitor->output->enabled) continue;

        monitor_update_geometry(monitor);
    }
}

void seat_set_cursor(struct wl_listener *listener, void *data) {
    struct wlr_seat_pointer_request_set_cursor_event *event = data;
    if (seat->pointer_state.focused_client == event->seat_client) wlr_cursor_set_surface(cursor, event->surface, event->hotspot_x, event->hotspot_y);
}

void seat_set_selection(struct wl_listener *listener, void *data) {
    struct wlr_seat_request_set_selection_event *event = data;
    wlr_seat_set_selection(seat, event->source, event->serial);
}

void seat_set_primary_selection(struct wl_listener *listener, void *data) {
    struct wlr_seat_request_set_primary_selection_event *event = data;
    wlr_seat_set_primary_selection(seat, event->source, event->serial);
}

void seat_set_request_start_drag(struct wl_listener *listener, void *data) {
    struct wlr_seat_request_start_drag_event *event = data;

    if (wlr_seat_validate_pointer_grab_serial(seat, event->origin, event->serial)) {
        wlr_seat_start_pointer_drag(seat, event->drag, event->serial);
    }
    else {
        wlr_data_source_destroy(event->drag->source);
    }
}

void seat_set_start_drag(struct wl_listener *listener, void *data) {
        struct wlr_drag *drag = data;
    if (!drag->icon) return;

    // NOTE: Depending on what happens in the backend this could be a bad idea.
    drag->icon->data = &wlr_scene_drag_icon_create(drag_icon, drag->icon)->node;
    listen(&drag->icon->events.destroy, &listeners.drag_icon_destroy, drag_icon_destroy);
}

void monitor_frame(struct wl_listener *listener, void *data) {
    //wlr_log(WLR_DEBUG, "monitor frame");
    struct timespec now;
    struct monitor *monitor = wl_container_of(listener, monitor, listeners.frame);

	/* TODO: Render if no XDG clients have an outstanding resize and are visible on this monitor. */
    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &toplevels, link) {
        if (toplevel->resize_serial && !toplevel->floating && !client_is_stopped(toplevel) && client_is_rendered(toplevel, monitor)) {
            wlr_log(WLR_DEBUG, "skipping commit");
            goto skip;
        }
    }

    if (!wlr_scene_output_commit(monitor->output_scene, NULL)) {
        wlr_log(WLR_ERROR, "Failed to commit wlr_scene_output");
    }

skip:
    if (!clock_gettime(CLOCK_MONOTONIC, &now)) {
        wlr_scene_output_send_frame_done(monitor->output_scene, &now);
    }
    else {
        wlr_log(WLR_ERROR, "Failed to get timespec");
    }
}

void monitor_destroy(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "monitor destroy");
    struct monitor *monitor = wl_container_of(listener, monitor, listeners.destroy);

    wl_list_remove(&monitor->listeners.frame.link);
    wl_list_remove(&monitor->listeners.destroy.link);
    wl_list_remove(&monitor->listeners.request_state.link);
    wl_list_remove(&monitor->listeners.frame_done.link);
    wl_list_remove(&monitor->link);
    wlr_output_layout_remove(output_layout, monitor->output);
    wlr_scene_output_destroy(monitor->output_scene);
    wlr_scene_node_destroy(&monitor->bar.scene->node);
    wlr_allocator_destroy(monitor->bar.swapchain.allocator);
    swapchain_destroy(&monitor->bar.swapchain);

    struct tag *tag, *tmp;
    wl_list_for_each_safe(tag, tmp, &monitor->tags, link) {
        wl_list_remove(&tag->link);
        free(tag);
    }

    if (selected_monitor == monitor) selected_monitor = NULL;

    // TODO: Move clients to another monitor
    free(monitor);
}

void monitor_request_state(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "monitor request state");
    struct wlr_output_event_request_state *event = data;
    wlr_output_commit_state(event->output, event->state);
    // TODO: Update monitors
}

void pointer_init(struct wlr_pointer *wlr_pointer) {
    // hopefully this allows us to have multiple pointers.
    wlr_cursor_attach_input_device(cursor, &wlr_pointer->base);

    struct pointer *pointer = ezalloc(sizeof(*pointer));
    pointer->pointer = wlr_pointer;
    if (wl_list_empty(&pointers)) wlr_cursor_set_xcursor(cursor, xcursor, "default");
    wl_list_insert(&pointers, &pointer->link);
    listen(&wlr_pointer->base.events.destroy, &pointer->listeners.destroy, pointer_destroy);
}

void pointer_destroy(struct wl_listener *listener, void *data) {
    struct pointer *pointer = wl_container_of(listener, pointer, listeners.destroy);
    wl_list_remove(&pointer->listeners.destroy.link);
    wl_list_remove(&pointer->link);
    if (wl_list_empty(&pointers)) {
        wlr_cursor_unset_image(cursor);
        seat_update_capabilities();
    }
    free(pointer);
}

void new_lock_surface(struct wl_listener *listener, void *data) {
        struct wlr_session_lock_surface_v1 *surface = data;
    struct monitor *monitor = surface->output->data;

    struct wlr_scene_tree *tree = wlr_scene_subsurface_tree_create(session_lock.surfaces, surface->surface);
    monitor->lock_surface = surface;

    wlr_scene_node_set_position(&tree->node, monitor->monitor_space.x, monitor->monitor_space.y);
    wlr_session_lock_surface_v1_configure(surface, monitor->monitor_space.width, monitor->monitor_space.height);

    listen(&surface->events.destroy, &monitor->listeners.lock_surface_destroy, lock_surface_destroy);

    if (monitor == selected_monitor) /*  TODO: client notify enter the lock surface */;
}

void session_lock_destroy(struct wl_listener *listener, void *data) {
    lock_destroy();
}

void unlock(struct wl_listener *listener, void *data) {
    lock_destroy();
}

void lock_destroy() {
    wl_list_remove(&listeners.lock_destroy.link);
    wl_list_remove(&listeners.new_lock_surface.link);
    wl_list_remove(&listeners.unlock.link);
    wlr_scene_node_set_enabled(&session_lock.surfaces->node, false);
    wlr_scene_node_set_enabled(&session_lock.background->node, false);
    session_lock.lock = NULL;
    // TODO: Damage whole of all outputs?
}

void lock_surface_destroy(struct wl_listener *listener, void *data) {
    struct monitor *monitor = wl_container_of(listener, monitor, listeners.lock_surface_destroy);
    struct wlr_session_lock_surface_v1 *surface = monitor->lock_surface, *next = NULL;

    wl_list_remove(&listener->link);
    monitor->lock_surface = NULL;

    // Refocus the to another lock surface. Otherwise clear focus.
    wl_list_for_each(next, &session_lock.lock->surfaces, link) {
        if (next != surface && next->surface->mapped) break;
    };
    if (next != surface && next->surface->mapped)  {
        // TODO: Client notify enter the new lock surface
    } else {
        wlr_seat_keyboard_clear_focus(seat);
    }
    // TODO: Damage whole output?
}

void toplevel_commit(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "toplevel commit");
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.commit);

    // xdg shell demands that we send the bounds and size of the toplevel.
    if (toplevel->xdg.surface->surface->mapped && toplevel->monitor) {
        client_resize(toplevel, &toplevel->geometry, &toplevel->monitor->toplevel_space);
    }

    if (toplevel->resize_serial && toplevel->resize_serial <= toplevel->xdg.surface->current.configure_serial) {
        toplevel->resize_serial = 0;
    }
}

void toplevel_destroy(struct wl_listener *listener, void *data){
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.destroy);
    switch (toplevel->base.type) {
        case CLIENT_XDG:
            wl_list_remove(&toplevel->listeners.commit.link);
            wl_list_remove(&toplevel->listeners.map.link);
            wl_list_remove(&toplevel->listeners.unmap.link);
            wl_list_remove(&toplevel->listeners.fullscreen.link);
            wl_list_remove(&toplevel->listeners.maximize.link);
            wl_list_remove(&toplevel->listeners.destroy.link);
            break;
        case CLIENT_X11:
            // NOTE: Might not need this
            break;
        default: return;
    }

    wlr_scene_node_destroy(&toplevel->scene->node);

    client_focus(NULL);
    monitor_arrange(toplevel->monitor);

    free(toplevel);
}

void toplevel_map(struct wl_listener *listener, void *data) {
    wlr_log(WLR_DEBUG, "toplevel map");
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.map);

    // TODO: Handle urgent stuff don't know where or when.

	client_set_tiled(toplevel, WLR_EDGE_TOP | WLR_EDGE_BOTTOM | WLR_EDGE_LEFT | WLR_EDGE_RIGHT);
    toplevel->geometry.width += (toplevel->border_width * 2);
    toplevel->geometry.height += (toplevel->border_width * 2);

    wlr_scene_node_set_enabled(&toplevel->scene->node, true);
    wl_list_insert(&toplevels, &toplevel->link);
    wl_list_insert(&focus_stack, &toplevel->focus_link);

    struct toplevel *parent;
    if (toplevel->base.type == CLIENT_XDG && (parent = client_get_parent(toplevel))) {
        wlr_log(WLR_DEBUG, "has parent should float");
        toplevel->floating = true;
        wlr_scene_node_reparent(&toplevel->scene->node, layers[FLOATING]);
        client_set_monitor(toplevel, parent->monitor, parent->tags);
        return;
    }

    client_apply_rules(toplevel);
}

void toplevel_unmap(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.unmap);

    wlr_scene_node_set_enabled(&toplevel->scene->node, false);
    wl_list_remove(&toplevel->link);
    wl_list_remove(&toplevel->focus_link);
}

void toplevel_fullscreen(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.fullscreen);
}

void toplevel_maximize(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.maximize);
	if (wl_resource_get_version(toplevel->xdg.surface->toplevel->resource)
			< XDG_TOPLEVEL_WM_CAPABILITIES_SINCE_VERSION)
		wlr_xdg_surface_schedule_configure(toplevel->xdg.surface);
}

struct monitor *monitor_from_position(uint32_t x, uint32_t y) {
    struct wlr_output *output = wlr_output_layout_output_at(output_layout, x, y);
    return output ? output->data : NULL;
}

//void window_draw(struct basic_window *window) {
//    struct monitor *monitor = wl_container_of(window, monitor, indicator);
//
//    wlr_log(WLR_DEBUG, "%p", scene);
//    wlr_log(WLR_DEBUG, "%p", window->scene_buffer->primary_output);
//    wlr_log(WLR_DEBUG, "%d %d", window->scene_buffer->node.x, window->scene_buffer->node.y);
//    wlr_log(WLR_DEBUG, "%d %d", window->scene->node.x, window->scene->node.y);
//
//    char *str = ecalloc(10, sizeof(*str));
//    for (int i = 0; i < length(tags); i++) str[i] = monitor->tagset[monitor->current_tagset] & (1 << i) ? '1' : '0';
//    str[9] = '\0';
//
//    wlr_log(WLR_INFO, "%s views: %s", monitor->output->name, str);
//
//    const uint32_t height = font->height + (border_width * 2);
//    const uint32_t width = text_width(font, str) + font->height + (border_width * 2);
//    struct wlr_buffer *buffer = basic_window_get_buffer(window, width, height);
//    assert(buffer);
//
//    void *data;
//    uint32_t format;
//    size_t stride;
//    if (!wlr_buffer_begin_data_ptr_access(buffer, WLR_BUFFER_DATA_PTR_ACCESS_WRITE, &data, &format, &stride)) {
//        wlr_log(WLR_ERROR, "Failed to render because we cannot access wlr_buffer");
//        free(str);
//        return;
//    }
//
//    pixman_image_t *image = pixman_image_create_bits(PIXMAN_a8r8g8b8, buffer->width, buffer->height, data, stride);
//    assert(image);
//
//    pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &PIXMAN_COLOR(cyan), 1, &(const pixman_box32_t){
//            .x1 = 0, .y1 = 0,
//            .x2 = buffer->width, .y2 = buffer->height,
//            });
//    pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &PIXMAN_COLOR(gray1), 1, &(const pixman_box32_t){
//            .x1 = border_width, .y1 = border_width,
//            .x2 = buffer->width - border_width, .y2 = buffer->height - border_width,
//            });
//
//    const struct wlr_box box = {
//        .x = (font->height / 2) + border_width,
//        .y = font->height - border_width + 1,
//        .height = height - border_width,
//        .width = width - (font->height / 2) - border_width,
//    };
//    text_draw(font, image, str, &PIXMAN_COLOR(gray3), &box);
//    free(str);
//    pixman_image_unref(image);
//    wlr_buffer_end_data_ptr_access(buffer);
//
//    wlr_log(WLR_DEBUG, "render done");
//}

uint32_t text_draw(struct fcft_font *font, pixman_image_t *image, const char *text, const pixman_color_t *text_color, const struct wlr_box *box) {
    if (!text) return 0;

    //wlr_log(WLR_DEBUG, "draw text");

    int64_t kern;
    uint32_t state = UTF8_ACCEPT, previous_codepoint = 0, codepoint, x = box->x == -1 ? 0 : box->x;
    pixman_image_t *text_image = image && text_color ? pixman_image_create_solid_fill(text_color) : NULL;
    for (; *text; text++) {
        if (utf8decode(&state, &codepoint, *text)) continue;

        const struct fcft_glyph *glyph = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
        if (!glyph) continue;

        kern = 0;
        if (previous_codepoint) fcft_kerning(font, previous_codepoint, codepoint, &kern, NULL);

        if (box->width != -1 && x + kern + glyph->advance.x > box->width) {
            //wlr_log(WLR_ERROR, "breaking");
            break;
        }

        if (text_image) {
            if (pixman_image_get_format(glyph->pix) == PIXMAN_a8r8g8b8) {
                pixman_image_composite32(PIXMAN_OP_OVER, glyph->pix, text_image, image,
                        0, 0, 0, 0,
                        x + glyph->x, box->y - glyph->y,
                        glyph->width, glyph->height);
            } else {
                pixman_image_composite32(PIXMAN_OP_OVER, text_image, glyph->pix, image,
                        0, 0, 0, 0,
                        x + glyph->x, box->y - glyph->y,
                        glyph->width, glyph->height);
            }
        }

        x += kern + glyph->advance.x;
        previous_codepoint = codepoint;
        //wlr_log(WLR_DEBUG, "draw text iter");
    }

    if (text_image) pixman_image_unref(text_image);

    return x;
}

uint32_t text_width(struct fcft_font *font, const char *text) {
    const struct wlr_box box = {.x = 0, .y = 0, .width = -1, .height = -1};
    return text_draw(font, NULL, text, NULL, &box);
}

void monitor_update_geometry(struct monitor *monitor) {
    if (!monitor) return;
    wlr_log(WLR_DEBUG, "update monitor geometry %s", monitor->output->name);

    struct wlr_box box;
    wlr_output_layout_get_box(output_layout, monitor->output, &box);
    monitor->monitor_space = box;
    monitor->toplevel_space = box;

    wlr_scene_output_set_position(monitor->output_scene, monitor->monitor_space.x, monitor->monitor_space.y);

    if (monitor->bar.scene->node.enabled) {
        monitor->toplevel_space.y += font->height + 2;
        wlr_scene_buffer_set_dest_size(monitor->bar.scene, monitor->monitor_space.width, font->height + 2);
    }
}

struct toplevel *monitor_get_top(struct monitor *monitor) {
    if (!monitor) return NULL;

    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &focus_stack, focus_link) {
        if (CLIENT_VISIBLE_ON(toplevel, monitor)) return toplevel;
    }

    return NULL;
}

void seat_update_capabilities(void) {
    uint32_t capabilites = 0;
    if (!wl_list_empty(&pointers))  capabilites |= WL_SEAT_CAPABILITY_POINTER;
    if (!wl_list_empty(&keyboards.group->devices) || !wl_list_empty(&virtual_keyboards.group->devices)) capabilites |= WL_SEAT_CAPABILITY_KEYBOARD;
    if (capabilites != seat->capabilities) wlr_seat_set_capabilities(seat, capabilites);
}

void monitor_arrange(struct monitor *monitor) {
    wlr_log(WLR_DEBUG, "monitor arrange");
    if (!monitor) return;

    const struct layout *layout = monitor->current_tag->layouts[monitor->current_tag->layout];
    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &toplevels, link) {
        if (toplevel->monitor != monitor) continue;

        wlr_scene_node_set_enabled(&toplevel->scene->node, CLIENT_VISIBLE_ON(toplevel, monitor));
        client_set_suspended(toplevel, !CLIENT_VISIBLE_ON(toplevel, monitor));
    }

    // TODO: Fullscreen stuff, after we actually implement fullscreen first

    if (layout->arrange) layout->arrange(monitor);
}

void monitor_update_tag(struct monitor *monitor) {
    if (!monitor) return;

    struct tag *tag;
    uint32_t i = 0;
    wl_list_for_each(tag, &monitor->tags, link) {
        if (!((1 << i) & monitor->tagset[monitor->current_tagset])) {
            i++;
            continue;
        }

        monitor->current_tag = tag;
        break;
    }

    wlr_log(WLR_DEBUG, "monitor update tag %d", monitor->current_tag->layout);

    wlr_scene_node_set_enabled(&monitor->bar.scene->node, monitor->current_tag->show_bar);
    if (monitor->output_scene) monitor_update_geometry(monitor);
}

struct monitor *monitor_from_xy(uint32_t x, uint32_t y) {
    struct wlr_output *output = wlr_output_layout_output_at(output_layout, x, y);
    return output ? output->data : NULL;
}

void quit(const union arg *arg) {
    wl_display_terminate(display);
}

void view(const union arg *arg) {
    struct monitor *monitor = selected_monitor;
    if (!monitor || (arg->ui & TAGMASK) == monitor->tagset[monitor->current_tagset]) return;

    monitor->current_tagset ^= 1;
    if (arg->ui & TAGMASK) monitor->tagset[monitor->current_tagset] = arg->ui & TAGMASK;

    monitor_update_tag(monitor);
    client_focus(NULL);
    monitor_arrange(monitor);
}

void toggle_view(const union arg *arg) {
    struct monitor *monitor = selected_monitor;
    uint32_t new_viewset;
    if (!monitor || !(new_viewset = monitor->tagset[monitor->current_tagset] ^ (arg->ui & TAGMASK))) return;
    monitor->tagset[monitor->current_tagset] = new_viewset;

    monitor_update_tag(monitor);
    client_focus(NULL);
    monitor_arrange(monitor);
}

int main(int argc, char **argv) {
    printf("main\n");
    wlr_log_init(WLR_DEBUG, NULL);

    setup();
    run();
    cleanup();

    return EXIT_SUCCESS;
}

void _panic(uint32_t line, const char *file, const char *fmt, ...) {
    printf("[%u][%s][yutani][panic] ", line, file);

    va_list list;
    va_start(list, fmt);
    vfprintf(stderr, fmt, list);
    va_end(list);
    fputc('\n', stderr);

    exit(EXIT_FAILURE);
}

void *ecalloc(size_t blocks, size_t size) {
    void *ptr = calloc(blocks, size);
    // NOTE: I don't really like this because it makes the panic a little pointless as it will
    // always show the line inside this function not the caller's line.
    //
    // TODO: Maybe wrap this function in a macro like with _panic? Then have it pass in the line we are on?
    // Seems hacky and a little stupid to me though. But like within boundaries.
    if (!ptr) panic("Allocation failed");
    return ptr;
}

void *ezalloc(size_t size) {
    return ecalloc(1, size);
}
