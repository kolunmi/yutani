# Yutani
A simple wayland compositor.

# TODO
+ [+] keys aren't getting forwarded to the client. This might have something to do with the focusing of clients
or the fact that we don't handle pointer motion.
+ [+] Use the server decorations protocol to stop clients from making their own decorations.
+ [+] Make tags actually work.
+ [+] Remove the state struct and move pretty much everything into main.c. Expand the state struct into globals at the top to make things simple
+ [+] Create the bar. Don't make a status thing just yet, we'll work on it. Status input is either going to be a socket/fifo file or through a protocol.
+ [+] Support the ability of clients to float. With keybinds and stuff.
+ [+] Add support for the xdg activation protocol.
+ [+] Add a vertical split tiling layout with the symbol '|||' which splits the clients evenly vertically
+ [ ] Get pointer stuff working better like motion handling resizing and moving and button binds.
+ [ ] Properly rename client based functions and stuff so that it's understood whether a function is working on the opaque struct client or the implemented client type (ie. toplevel, or layered).
+ [ ] Move all of the client.* file stuff to main.c same thing with window.h
+ [ ] Consider moving all of the user.h stuff to main.c then just make config.h include into main.c in the same way dwm does.
+ [ ] Fully support the xdg shell protocol and fullscreen protocol
+ [ ] Add support for the keyboard shortcuts protocol
+ [ ] Add support for the relative pointer and pointer constraints protocols
+ [ ] Status in the bar.
+ [ ] Change up the names for everything to be much shorter than they are.
+ [ ] Move layout symbol to struct monitor from struct tag. This could save some memory considering we end up overwriting it most of the time anyway.
