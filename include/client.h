#ifndef CLIENT_H_
#define CLIENT_H_

#include <stdint.h>
#include <wayland-util.h>
#include <wlr/types/wlr_xdg_decoration_v1.h>
#include <wlr/util/box.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_keyboard.h>
#include "wlr-layer-shell-unstable-v1-protocol.h"
#include "window.h"

enum client_type {
    CLIENT_XDG,
    CLIENT_X11,
    CLIENT_LAYERED,
};

struct client {
    struct window base; // You've got to love multiple levels of indirection!
    enum client_type type;
};

struct toplevel {
    struct client base;
    struct monitor *monitor;
    union {
        struct {
            struct wlr_xdg_surface *surface;
            struct wlr_xdg_toplevel_decoration_v1 *decoration;
        } xdg;
        struct {
            struct wlr_xwayland_surface *surface;
        } x11;
    };

    uint32_t tags, resize_serial, border_width;

    struct wlr_box geometry, bounds;
    struct wlr_scene_tree *scene,     // The scene tree for the whole client
                          *surface;   // The tree for the client surface.
    struct wlr_scene_rect *border[4]; // The border rectangles for the client.

    bool floating, urgent, fullscreen;

    struct {
        struct wl_listener commit;
        struct wl_listener map;
        struct wl_listener unmap;
        struct wl_listener fullscreen;
        struct wl_listener maximize;
        struct wl_listener destroy;
        struct wl_listener decoration_request;
        struct wl_listener decoration_destroy;
    } listeners;

    struct wl_list link, focus_link; // Are only used with xwayland and xdg client.
};

struct layered {
    struct client base;
    struct monitor *monitor;
    enum zwlr_layer_shell_v1_layer layer;
    struct wlr_layer_surface_v1 *layer_surface;

    struct {

    } listeners;

    struct wl_list link;
};

bool root_client_from_surface(struct wlr_surface *surface, enum client_type *type, struct layered **layered, struct toplevel **toplevel);

bool client_is_x11(struct toplevel *toplevel);
bool client_is_xdg(struct toplevel *toplevel);
bool client_is_rendered(struct toplevel *toplevel, struct monitor *monitor);
bool client_is_stopped(struct toplevel *toplevel);
bool client_wants_focus(struct toplevel *toplevel);

// A shorthand description of an x11 thing where a client doesn't want the compositor to mess with this surface.
// Or might not want them to at least.
// See: https://tronche.com/gui/x/xlib/window/attributes/override-redirect.html
bool client_is_unmanaged(struct toplevel *toplevel);

struct wlr_surface *client_get_surface(struct toplevel *toplevel);
struct toplevel *client_get_root(struct toplevel *toplevel);
bool client_get_geometry(struct toplevel *toplevel, struct wlr_box *box);
bool client_get_clip(struct toplevel *toplevel, struct wlr_box *box);
const char *client_get_title(struct toplevel *toplevel);
const char *client_get_appid(struct toplevel *toplevel);
struct toplevel *client_get_parent(struct toplevel *toplevel);
bool client_has_parent(struct toplevel *toplevel);

bool client_restack(struct toplevel *toplevel);
bool client_set_border(struct toplevel *toplevel, uint32_t color);
bool client_set_tiled(struct toplevel *toplevel, uint32_t tiled);
bool client_set_activated(struct toplevel *toplevel, bool activated);
bool client_set_suspended(struct toplevel *toplevel, bool suspended);
bool client_set_bounds(struct toplevel *toplevel, uint32_t width, uint32_t height);
int32_t client_set_size(struct toplevel *toplevel, uint32_t width, uint32_t height);
bool client_send_close(struct toplevel *toplevel);

#endif // CLIENT_H_
