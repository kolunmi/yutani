#include "buffer.h"
#include "util.h"
#include <stdint.h>
#include <stdlib.h>
#include <drm_fourcc.h>
#include <wayland-util.h>
#include <wlr/interfaces/wlr_buffer.h>
#include <wlr/util/log.h>

static struct wlr_buffer *allocator_create_buffer(struct wlr_allocator *allocator,
        int32_t width, int32_t height, const struct wlr_drm_format *format);
static void allocator_destroy(struct wlr_allocator *allocator);
static void buffer_destroy(struct wlr_buffer *buffer);
static bool buffer_begin_access(struct wlr_buffer *buffer, uint32_t flags, void **data, uint32_t *format, size_t *stride);
static void buffer_end_access(struct wlr_buffer *buffer);

static const struct wlr_allocator_interface allocator_interface = {
    .destroy = allocator_destroy,
    .create_buffer = allocator_create_buffer,
};

static const struct wlr_buffer_impl buffer_interface = {
    .destroy = buffer_destroy,
    .begin_data_ptr_access = buffer_begin_access,
    .end_data_ptr_access = buffer_end_access,
    .get_shm = NULL, .get_dmabuf = NULL,
};

struct wlr_allocator *buffer_allocator_create(void) {
    struct buffer_allocator *allocator = ezalloc(sizeof(*allocator));
    wlr_allocator_init(&allocator->base, &allocator_interface, WLR_BUFFER_CAP_DATA_PTR);
    return &allocator->base;
}

struct wlr_buffer *allocator_create_buffer(struct wlr_allocator *allocator, int32_t width, int32_t height, const struct wlr_drm_format *format) {
    if (width <= 0 || height <= 0 || !format) return NULL;

    struct buffer *buffer = buffer_create(width, height, format);
    return buffer ? &buffer->base : NULL;
}

void allocator_destroy(struct wlr_allocator *wlr_allocator) {
    struct buffer_allocator *allocator = wl_container_of(wlr_allocator, allocator, base);
    free(allocator);
}

struct buffer *buffer_create(uint32_t width, uint32_t height, const struct wlr_drm_format *format) {
    if (!format || format->format != DRM_FORMAT_ARGB8888) return NULL;

    struct buffer *buffer = ezalloc(sizeof(*buffer));

    buffer->bytes_per_block = 4;
    buffer->format = format->format;
    buffer->stride = buffer->bytes_per_block * width;
    buffer->data = ecalloc(height, buffer->stride);

    wlr_buffer_init(&buffer->base, &buffer_interface, width, height);

    return buffer;
}

void buffer_destroy(struct wlr_buffer *wlr_buffer) {
    struct buffer *buffer = wl_container_of(wlr_buffer, buffer, base);
    free(buffer->data);
    free(buffer);
}

// WARN: We might want to make this read access only.
// But in that case we might not be able to have the allocator.
bool buffer_begin_access(struct wlr_buffer *wlr_buffer, uint32_t flags, void **data, uint32_t *format, size_t *stride) {
    struct buffer *buffer = wl_container_of(wlr_buffer, buffer, base);
    if (data) *data = buffer->data;
    if (format) *format = buffer->format;
    if (stride) *stride = buffer->stride;
    return true;
}

void buffer_end_access(struct wlr_buffer *wlr_buffer) {
    // Nothing to do here.
}
